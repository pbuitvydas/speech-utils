package lt.vdu.speech.utils.mlfdiff.grouping;

public class TransitionInfo {
	private int count;
	private double proba;
	
	public TransitionInfo() {
		count = 1; //nes jei kuri reiskias desi nauja kurio hitCount bus 1
		proba = 0;
	}
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public double getProba() {
		return proba;
	}
	public void setProba(double proba) {
		this.proba = proba;
	}
}
