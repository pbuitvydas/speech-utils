package lt.vdu.speech.utils.mlfdiff.wrapper.html;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import lt.vdu.speech.utils.common.diff.HtmlDiffWrapper;
import lt.vdu.speech.utils.common.dstructures.Mlf;
import lt.vdu.speech.utils.common.io.writer.BaseWriter;
import lt.vdu.speech.utils.common.utilities.ClassUtils;
import lt.vdu.speech.utils.mlfdiff.data.Change;
import difflib.Chunk;
import difflib.Delta;
import difflib.Delta.TYPE;
import difflib.Patch;

/**
 * TODO pakomentuot
 * @author pbuitvydas
 *
 * @param <T>
 */
public class MlfDiffToHtmlWrapper<T extends Mlf> extends HtmlDiffWrapper<T> {
	
	public static final String JS_SCRIPT_FILE = "launch-praat.js";
	
	public static final List<String> STOP_PHONES = new ArrayList<String>() {
		{
			add("sill");
			add("sil");
			add(".");
			add("sp");
		}
	};
	
	@Override
	public void wrapBody(BaseWriter<?,?> writer, Patch patch) {
		List<Delta> deltas = patch.getDeltas();
		wrapBody(writer, deltas);
	}
	

	@Override
	public String bodyBegin(Patch patch) {
		String script = "<body>\n\t<script type='text/javascript'>\n";
    	script +=
    			"function launch(start, end, lab) {"
    			+ "w = new ActiveXObject(\"WScript.Shell\");"
    			+ "w.run('java -jar D:/Speech/tools/htk/tescal/subgrid.jar -mode intervals -start ' + start + ' -end ' + end + ' -cfg D:/Speech/tools/htk/tescal/subGrid.cfg  -lab ' + lab);"
    			+ "}"
    			+ "</script>";
		script += "\n\t</script>";
		return script;
	}
	
	protected String formPraatLink(Transcription transcription) {
		T start = transcription.transcriptionList.get(0).getMlf();
		T end = transcription.transcriptionList.get(transcription.transcriptionList.size() -1).getMlf();
		return formPraatLink(start, end, transcription.getLabName());
	}
	
	protected String formPraatLink(T start, T end, String labName) {
		return "";
	}
	
	@Override
	public String getRValue(List<T> contentList, Chunk chunk, TYPE type) {
		Transcription transcription = getTranscription(contentList, chunk.getPosition(), type);
		String retVal = transcription.transcriptionToString();
		retVal += formPraatLink(transcription);
		return retVal;
	}
	
	@Override
	public String getLValue(List<T> contentList, Chunk chunk, TYPE type) {
		return getTranscription(contentList, chunk.getPosition(), type).transcriptionToString();
	}
	
	public Transcription getTranscription(List<T> mlfList, int position, TYPE changeType) {
    	List<T> list = mlfList;
    	boolean useContext = true;
    	int i, start = -1, end = -1, wordStartsAt, wordEndsAt;
    	start = getWordStartPos(list, position, position);
    	wordStartsAt = start;
    	wordEndsAt = STOP_PHONES.contains(list.get(position).getItem()) ? position-1 : position;
    	wordEndsAt = end = getWordEndPosition(list, wordEndsAt, wordEndsAt);

    	//TODO pergalvot
    	if (useContext) {
        	start = getWordStartPos(list, start-1, start);
        	end = getWordEndPosition(list, end+1, end);
    	}

    	List<Change> transList = new ArrayList<Change>();
    	Transcription transcription = new Transcription();
    	for (i = start; i < end+1; i++) {
    		T mItem = list.get(i);
    		transcription.score += getScore(mItem);
    		Change change = new Change(mItem, (i == position) ? changeType : null);
    		transList.add(change);
    	}
    	transcription.setTranscription(transList);
    	transcription.setLabName(getLabName(mlfList, position));
    	return transcription;
	}
	
	/**
	 * gauti svorį
	 * @param item
	 * @return
	 */
	public double getScore(T item) {
		//mlf nenaudojamas ir visad grazina 0
		return 0;
	}
	
	//TODO patikrinimu reika daugiau luzta
	// iki sp arba artimiausio deleto
	//arba ziuret kas pries tai buvo originaliam liste
    public int getWordStartPos(List<T> list, int position, int deflt) {
    	if (position < list.size()) {
    		position = deflt;
    	}
    	if (STOP_PHONES.contains(list.get(position).getItem())) {
    		position--;
    	}
    	for (int i = position; i != 0; i--) {
    		Mlf m = list.get(i);
    		if (STOP_PHONES.contains(m.getItem()) || m.getItem().contains(".lab")) {
    			return (i < list.size()-1) ? ++i : i;
    		}
    	}
    	return deflt;
    }
    
    public int getWordEndPosition(List<T> list, int position, int deflt) {
    	if (STOP_PHONES.contains(list.get(position).getItem())) {
    		position++;
    	}
    	for (int i = position; i < list.size(); i++) {
    		Mlf m = list.get(i);
    		if (STOP_PHONES.contains(m.getItem())) {
    			return i > 0 ? --i : i;
    		}
    	}
    	return deflt;
    }
    
    public String getLabName(List<T> list, int position) {
    	return list.get(position).getLabName();
    }
    
    public class Change {
    	private T change;
    	private TYPE type;
    	
    	public Change(T change, TYPE type) {
    		this.change = change;
    		this.type = type;
    	}
    	
    	public T getMlf() {
    		return change;
    	}
    	
    	public TYPE getType() {
    		return type;
    	}
    }
    
    public class Transcription {
    	private List<Change> transcriptionList = new ArrayList<MlfDiffToHtmlWrapper<T>.Change>();
    	private String labName;
    	private double score;
    	
		public List<Change> getTranscription() {
			return transcriptionList;
		}
		public void setTranscription(List<Change> transcription) {
			this.transcriptionList = transcription;
		}
		public String getLabName() {
			return labName;
		}
		public void setLabName(String labName) {
			this.labName = labName;
		}
		public double getScore() {
			return score;
		}
		public void setScore(double score) {
			this.score = score;
		}
		
		public Long getStart() {
			if (transcriptionList.size() > 0) {
				return transcriptionList.get(0).getMlf().getStart2();
			}
			return null;
		}
		
		public Long getStop() {
			if (transcriptionList.size() > 0) {
				return transcriptionList.get(transcriptionList.size()-1).getMlf().getStop2();
			}
			return null;
		}
		
		//TODO sulipdyt iskarto
		public String transcriptionToString() {
			String retVal = "";
			for (Change change : transcriptionList) {
				if (change.getType() != null) {
					retVal += "<span style='" + getColor(change.getType()) +";'>" + change.getMlf().getItem() + "</span> ";
				} else {
					retVal += change.getMlf().getItem() + " ";
				}
			}
			return retVal;
		}
		
		public String toString() {
			return transcriptionToString();
		}
		
		public int indexOfTeeModel() {
			for (int i = 0; i < transcriptionList.size(); i++) {
				Change chg = transcriptionList.get(i);
				if (STOP_PHONES.contains(chg.getMlf().getItem())) {
					return i;
				}
			}
			return -1;
		}
		
		public boolean containsModel(String model) {
			for (Change ch : transcriptionList) {
				if (model.equals(ch.change.getItem())) {
					return true;
				}
			}
			return false;
		}
    	
    }
    
    
    /**
     * Diff comparatorius, naudojamas surūšiuoti
     * @author pbuitvydas
     *
     */
    public class DeltaComparator implements Comparator<Delta> {

		public int compare(Delta a, Delta b) {
			//AO - AOriginal, AR - ARevised
			Transcription transcriptionAO = getTranscription(getOriginal(), a.getOriginal().getPosition(), a.getType());
			Transcription transcriptionBO = getTranscription(getOriginal(), b.getOriginal().getPosition(), b.getType());
			Transcription transcriptionAR = getTranscription(getRevised(), a.getRevised().getPosition(), a.getType());
			Transcription transcriptionBR = getTranscription(getRevised(), b.getRevised().getPosition(), b.getType());
			double diffA = transcriptionAO.score - transcriptionAR.score;
			double diffB = transcriptionBO.score - transcriptionBR.score;
			double diff = diffB - diffA;
			if (diff > 0) {
				return 1;
			}
			if (diff < 0) {
				return -1;
			}
			return 0;
		}
    	
    }

}
