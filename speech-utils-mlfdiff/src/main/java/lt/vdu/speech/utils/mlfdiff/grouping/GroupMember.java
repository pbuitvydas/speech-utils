package lt.vdu.speech.utils.mlfdiff.grouping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lt.vdu.speech.utils.mlfdiff.data.TranscriptionHolder;

import difflib.Delta;
import difflib.Delta.TYPE;

/**
 * Pakeitimų grupavimo entitis
 * reikalingas suskirtsyti pakeitimus i grupes pvz
 * group : member
 * g1 : s	-> z, Z, S
 * g1 : z	-> s, z, Z
 * g1 : ts	-> ...
 * @author pbuitvydas
 *
 */
public class GroupMember {
	
	private String groupName;
	
	private List<Matcher> matcherList = new ArrayList<Matcher>();

	private List<Integer> indexList = new ArrayList<Integer>();
	
	public static final String DEFAULT_GROUP = "rest";
	public static final String UNDEFINED_RULE = "<UNDEFINED>";
	
	public GroupMember() {
		
	}
	
	public GroupMember(String groupName, String matchRule, Object ...params) {
		this.groupName = groupName;
		createMatcher(matchRule, params);
	}
	
	public static GroupMember createDefaultGroupMember() {
		GroupMember m = new GroupMember(DEFAULT_GROUP, UNDEFINED_RULE);
		return m;
	}
	
	public boolean addIndex(int idx) {
		return indexList.add(idx);
	}
	
	public boolean isMemberOf(TranscriptionHolder holder) {
		for (Matcher matcher : matcherList) {
			if (matcher.matches(holder)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean createMatcher(String matchRule, Object ...params) {
		String[] splits = matchRule.split(" ");
		for (String str : splits) {
			Matcher matcher = MatcherFactory.create();
			matcher.setup(str, params);
			matcherList.add(matcher);
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "groupName=" + groupName + ", memberCount=" + indexList.size() + ", matchers=" + matcherList;
	}

	/**
	 * Indexai, kur grupės memberiai randasi DiffLib.deltaList'e
	 * @return
	 */
	public List<Integer> getIndexList() {
		return indexList;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

}
