package lt.vdu.speech.utils.mlfdiff.consts;

public final class C {

	public static final String activeGroupManager = "activeGroupManager";
	public static final String mlfFinderPattern = "mlfFinder/pattern";
	public static final String encodingCharset = "encoding/charset";
	
	public static final class GroupManager {
		public static final String tag = "groupManager";
		public static final String id = "@id";
		public static final String matcher = "matcher";
		
		public static String getByIdXpath(String idValue) {
			return String.format("//%s[%s='%s']", tag, id, idValue);
		}
	}
	
	public static final class Group {
		public static final String tag = "group";
		public static final String name = "@name";
		public static final String rule = "rule";
		public static final String params = "params";
	}

}
