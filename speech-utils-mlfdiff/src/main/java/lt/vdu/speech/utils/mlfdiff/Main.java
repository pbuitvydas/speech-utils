package lt.vdu.speech.utils.mlfdiff;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipFile;

import lt.vdu.speech.utils.common.diff.CommonDiff;
import lt.vdu.speech.utils.common.dstructures.Mlf;
import lt.vdu.speech.utils.common.encoding.Encoding;
import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;
import lt.vdu.speech.utils.common.global.Context;
import lt.vdu.speech.utils.common.global.FrameConverter;
import lt.vdu.speech.utils.common.io.reader.MlfReader;
import lt.vdu.speech.utils.common.io.writer.BasicBufferedWriter;
import lt.vdu.speech.utils.common.io.writer.BasicWriterStdOut;
import lt.vdu.speech.utils.common.utilities.ClassUtils;
import lt.vdu.speech.utils.common.utilities.FileFilterByPattern;
import lt.vdu.speech.utils.common.utilities.FileUtils;
import lt.vdu.speech.utils.mlfdiff.accept.ChangesAcceptor;
import lt.vdu.speech.utils.mlfdiff.consts.C;
import lt.vdu.speech.utils.mlfdiff.exception.MlfDiffException;
import lt.vdu.speech.utils.mlfdiff.grouping.GroupManager;
import lt.vdu.speech.utils.mlfdiff.grouping.MatcherFactory;
import lt.vdu.speech.utils.mlfdiff.wrapper.html.MlfDiffToHtmlWrapper;
import lt.vdu.speech.utils.mlfdiff.wrapper.textgrid.TextGridwrapper;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.tree.xpath.XPathExpressionEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import difflib.Delta;
import difflib.Delta.TYPE;

public class Main {
	
	private static final Logger log = LoggerFactory.getLogger(Main.class);
	
	private static XMLConfiguration conf;
	public static final String SPEAKERS = "ARM BRU BUC CEK DEK DOV JOK JUK LAT LIP LUK MIC NAS RUP SAA SAD SAK SAL URB VID ADO ANI BLA BRA BRD BRO BUL BUT CIZ EID GAD KAM KAR KAZ KOS KUB LEO LIU MAK MAL MAR MEI PLA RAD RAV ROS SIR SIT SKA SMA";
	public static final boolean USEDELETE = true;
	//arg[0] original mlf file
	//arg[1] revised mlf file
	//arg[3] textGrid output folder
 	public static void main(String[] args) { 		
 		
 		if (args.length == 0) {
 			//diffTest(args);
 			printHelp();
 		} else if (args[0].equals("diff")) {
 			setup();
 			diff(args);
 		} else if (args[0].equals("accept")) {
 			setup();
 			accept(args);
 		} else if (args[0].equals("apply")) {
 			setup();
 			apply(args);
 		}

 	}
 	
 	public static void printHelpApply() {
 		System.out.println("mlf changes applier, aplies recout changes to structure mlf");
 		System.out.println("usage:");
 		System.out.println("structure.mlf recout.mlf aplied.mlf");
 	}
 	
 	public static void printHelpAccept() {
 		System.out.println("mlf diff acceptor accepts changes by given epsilon treshold");
 		System.out.println("diff usage:");
 		System.out.println("[rep del] original.mlf changed.mlf accepted.mlf [epsilon | acept | reject]");
 	}
 	
 	public static void printHelpDiff() {
 		System.out.println("mlf differ diffs two mlf files and wraps generated chages to textGridFormt");
 		System.out.println("\nUSAGE:");
 		System.out.println(" diff [original_dir | original.mlf] [changed_dir | changed.mlf] output_dir [groupManager]");
 		System.out.println("\nSYNOPSIS:");
 		System.out.println(" if directory name is suplied then application will search for mlf's by rules provided in mldiff.xml");
 		System.out.println(" groupManager - parameter defines which groupManager to use when grouping changes");
 		System.out.println(" for availiable group managers see mlfdiff.xml");
 		System.out.println("\nEXAMPLES:");
 		System.out.println(" diff original\\DEK_A.mlf modified\\DEK_A.mlf changes");
 		System.out.println(" diff original\\DEK_A.mlf modified\\DEK_A.mlf changes singleGroupManager");
 	}
 	
 	public static void printHelp() {
 		System.out.println("mlfdiff usage:");
 		System.out.println("mldiff [diff | accept | apply] <PARAMS>");
 		System.out.println("for help type <MODE> --help");
 		System.out.println("eg. accept --help");
 	}
 	
 	public static void setup() {
		try {
			URL confFile = Main.class.getClassLoader().getResource("conf/mlfdiff.xml");
			if (confFile == null) {
				log.warn("configuration file conf/mlfdiff.xml not found trying to load embedded configuration...");
				confFile = Main.class.getClassLoader().getResource("conf-default/mlfdiff.xml");
			}
			conf = new XMLConfiguration(confFile);
	    	conf.setExpressionEngine(new XPathExpressionEngine());
		} catch (ConfigurationException e) {
			ExceptionHandler.getInstance().handle(e);
		}
		Encoding.setCharsetName(conf.getString(C.encodingCharset));
 		Context.init();
 		Context.getInsTance().setFrameConverter(new FrameConverter(10000000));
 		Context.getInsTance().setGlobal(initGroup(conf));
 	}
 	
 	public static void diff(String[] args) {
 		if (args.length < 4 || args.length > 5) {
 			printHelpDiff();
 		} else {
 			String originalMlf = args[1];
 			String revisedMlf = args[2];
 			String outputDir = args[3];
 			if (args.length == 5) {
 				Context.getInsTance().setGlobal(initGroup(conf, args[4]));
 			}
 			if (new File(originalMlf).isDirectory()) {
 				try {
					process(originalMlf, revisedMlf, outputDir);
				} catch (MlfDiffException e) {
					ExceptionHandler.getInstance().handle(e);
				}
 			} else {
 				String speaker = FileUtils.getNameWithoutExtension(new File(originalMlf));
	 			Context.getInsTance().setSpeaker(speaker);
	 			File file = new File(outputDir + "/" + speaker + "_A");
	 			Context.getInsTance().setOutputDir(file);
	 			
	 	    	MlfReader originalReader = cretaeReader(originalMlf);
	 	    	MlfReader revisedReader = cretaeReader(revisedMlf);
	 	    	CommonDiff<Mlf> differ = new CommonDiff<Mlf>();
	 	    	differ.diff(originalReader, revisedReader);
	 	    	originalReader.close();
	 	    	revisedReader.close();
	
	 	    	TextGridwrapper tgw = new TextGridwrapper();
	 	    	
	 	    	differ.wrapResult(new BasicWriterStdOut<Mlf>(), tgw);
 			}
 	 		
 		}
 	}
 	
 	public static MlfReader cretaeReader(String fileName) {
 		if (fileName.contains(".zip")) {
 			try {
				ZipFile zipFile = new ZipFile(fileName);
				InputStream inStream = zipFile.getInputStream(zipFile.entries().nextElement());
				return new MlfReader(inStream);
			} catch (IOException e) {
				e.printStackTrace();
			}
 		}
 		return new MlfReader(fileName);
 	}
 	
 	public static MlfReader createReaderTest(String fileName) {
 		File f = new File(fileName);
 		if (!f.exists()) {
 			return cretaeReader(fileName.replace("mlf", "zip"));
 		}
 		return cretaeReader(fileName);
 	}
 	
 	public static void apply(String[] args) {
 		//TODO add logging
 		if (args.length != 4) {
 			printHelpApply();
 		} else {
 			String structureMlf = args[1];
 			String recoutMlf = args[2];
 			String outMlf = args[3];
 	 		
 	    	MlfReader originalReader = new MlfReader(structureMlf);
 	    	MlfReader revisedReader = new MlfReader(recoutMlf);
 	    	CommonDiff<Mlf> differ = new CommonDiff<Mlf>();
 	    	List<Mlf> originalList = originalReader.readAsList();
 	    	differ.diff(originalList, revisedReader.readAsList());
 	    	originalReader.close();
 	    	revisedReader.close();
 	    	List<Mlf> r = differ.patch();
 	    	wordsPerkelt(differ, r);
 	    	BasicBufferedWriter<Mlf> writer = new BasicBufferedWriter<Mlf>(outMlf) {
 	    		@Override
 	    		public void write(Mlf item) {
 	    			try {
 	    				String ret = "";
 	    				ret += item.getItem();
 	    				ret += item.getRight() != null ? " " + item.getRight() : "";
 	    				writeString(ret + newLine());
 	    			} catch (Exception e) {
 	    				ExceptionHandler.getInstance().handle(e);
 	    			}
 	    		}
 	    	};
 	    	writer.write(r);
 	    	writer.close();
 		}
 	}
 	
 	public static void diffTest(String[] args) {
		String rootDir = null;
		if (args.length == 0) {
			rootDir = "D:/speechChangesMake/debug";
		} else {
			rootDir = args[0];
		}
 		String orgRelative = "/original";
 		String modRelative = "/modified";
 		String chgRelative = "/changes";
 		String orgFull = rootDir + orgRelative;
 		String modFull = rootDir + modRelative;
 		String chgFull = rootDir + chgRelative;
 		setup();
 		try {
 			process(orgFull, modFull, chgFull);
		} catch (Exception e) {
			ExceptionHandler.getInstance().handle(e);
		}
 		
 	}
 	
 	public static void wordsPerkelt(CommonDiff<Mlf> differ, List<Mlf> lst) {
 		for (Delta d : differ.getPatch().getDeltas()) {
 			stringToAd(d, lst);
 		}
 	}
 	
 	public static void stringToAd(Delta delta, List<Mlf> changed) {
 		List<Mlf> originalLines = ClassUtils.unwild(delta.getOriginal().getLines());
 		String str = getRight(originalLines);
 		if (delta.getType().equals(TYPE.CHANGE)) {
 			changed.get(delta.getRevised().getPosition()).setRight(str);
 		} else {
 			if (str.length() > 0) {
 				Mlf mlf = changed.get(delta.getRevised().getPosition()-1);
 				if (MlfDiffToHtmlWrapper.STOP_PHONES.contains(mlf.getItem())) {
 					mlf = changed.get(delta.getRevised().getPosition());
 				}
 				mlf.setRight(str);
 			}
 		}

 		
 	}
 	
 	public static String getRight(List<Mlf> list) {
 		for (Mlf r : list) {
 			if (r.getRight() != null && !r.getRight().contains("sp")) {
 				return r.getRight();
 			}
 		}
 		return "";
 	}
 	
 	public static void accept(String[] args) {
 		if (args.length != 6) {
 			printHelpAccept();
 		} else {
 			String originalMlf = args[2];
 			String revisedMlf = args[3];
 			String acceptedMlf = args[4];
 			double epsilon = Double.parseDouble(args[5]);
 	    	MlfReader originalReader = new MlfReader(originalMlf);
 	    	List<Mlf> originaList = originalReader.readAsList();
 	    	MlfReader revisedReader = new MlfReader(revisedMlf);
 	    	List<Mlf> revisedList = revisedReader.readAsList();
 	    	if (args[1].equals("del")) {
 	    		revisedList = findDeletes(originaList, revisedList);
 	    		BasicBufferedWriter<Mlf> writer = new BasicBufferedWriter<Mlf>(revisedMlf);
 	    		writer.write(revisedList);
 	    		writer.close();
 	    	}
 	    	CommonDiff<Mlf> differ = new CommonDiff<Mlf>();
 	    	differ.diff(originaList, revisedList);
 	    	originalReader.close();
 	    	revisedReader.close();
 	    	
 	    	ChangesAcceptor ca = new ChangesAcceptor(epsilon, acceptedMlf);
 	    	ca.acceptRejectChanges(differ);
 		}
 	}
 	
 	public static List<Mlf> findDeletes(List<Mlf> originaList, List<Mlf> revisedList) {
 		List<Mlf> ret = new ArrayList<Mlf>();
 		if (originaList.size() == revisedList.size()) {
 			for (int i = 0; i < originaList.size(); i++) {
 				Mlf original = originaList.get(i);
 				Mlf revised = revisedList.get(i);
 				if (original.getItem().equals(revised.getItem()) && original.getProba() != 0 && revised.getProba() == 0) {
 					log.debug("found delete for {} {}", original, revised);
 					continue;
 				}
 				ret.add(revised);
 			}
 		}
 		return ret;
 	}
 	
 	public static void process(String originalDir, String revisedDir, String outPutDir) throws MlfDiffException {
 		Map<String, File> orgSpeakers = getMlfFileMap(originalDir);
 		Map<String, File> revSpeakers = getMlfFileMap(revisedDir);
 		for (String speaker : orgSpeakers.keySet()) {
 			File orgFile = orgSpeakers.get(speaker);
 			File revFile = revSpeakers.get(speaker);
 			if (revFile == null) {
 				throw new MlfDiffException(String.format("Unable to find matching revised speaker file %s in revDir=%s"
 						, orgFile.getPath(), revisedDir));
 			}
 			Context.getInsTance().setSpeaker(speaker);
 			String originalMlf = orgFile.getPath();
 			String revisedMlf = revFile.getPath();
 			File file = new File(outPutDir + "/" + speaker);
 			Context.getInsTance().setOutputDir(file);
 			doDiff(originalMlf, revisedMlf);
 		}
 	}
 	
 	public static Map<String, File> getMlfFileMap(String searchDir) {
 		Map<String, File> map = new HashMap<String, File>();
 		File file = new File(searchDir);
 		FileFilterByPattern filter = new FileFilterByPattern(conf.getString(C.mlfFinderPattern));
 		for (File f : file.listFiles(filter)) {
 			map.put(FileUtils.getNameWithoutExtension(f), f);
 		}
 		return map;
 	}
 	
 	public static void doDiff(String originalMlf, String revisedMlf) {
 		MlfReader originalReader = createReaderTest(originalMlf);
 		MlfReader revisedReader = createReaderTest(revisedMlf);
    	CommonDiff<Mlf> differ = new CommonDiff<Mlf>();
    	differ.diff(originalReader, revisedReader);
    	originalReader.close();
    	revisedReader.close();

    	TextGridwrapper tgw = new TextGridwrapper();
    	
    	differ.wrapResult(new BasicWriterStdOut<Mlf>(), tgw);
 	}
 	
 	public static void test(CommonDiff<Mlf> differ) {
 		List<Delta> tempList = new ArrayList<Delta>();
 		for (int i = 0; i < differ.getPatch().getDeltas().size(); i++) {
 			if (i%2==0) {
 				tempList.add(differ.getPatch().getDeltas().get(i));
 			}
 		}
 		differ.getPatch().getDeltas().clear();
 		differ.getPatch().getDeltas().addAll(tempList);
 		List<Mlf> patched = differ.patch();
 		BasicBufferedWriter<Mlf> writer = new BasicBufferedWriter<Mlf>("test.mlf");
 		writer.write(patched);
 		writer.close();
 	}
 	
 	
 	
 	public static GroupManager getGroupManager() {
 		return (GroupManager) Context.getInsTance().getGlobal();
 	}
 	
	public static GroupManager initGroup() {
		//grupavimas pagal original -> revised, pagal score???, universaliai
		GroupManager gm = new GroupManager();
		gm.addGroup("exclude", ".+_.+", TYPE.CHANGE);
		gm.addGroup("minkst", ".+\\\'", TYPE.CHANGE);
		gm.addGroup("s_z", "[szSZ]|ts|tS");
		gm.addGroup("k_g", "[kg]");
		gm.addGroup("t_d", "[td]");
		gm.addGroup("p_b", "[pb]");
		//galima ir tarpa naudot isvardinant visus galimus variantus
		gm.addGroup("dz_dZ", "dz dZ");
		gm.addGroup("G_x", "[Gx]");
		gm.addGroup("v_f", "[vf]");
		gm.addGroup("lmnr", "\\^?[lmnr]\\.?");
		gm.addGroup("s_z", "([szSZ]|ts|tS)'", TYPE.DELETE);
		gm.addGroup("k_g", "[kg]'", TYPE.DELETE);
		gm.addGroup("t_d", "[td]'", TYPE.DELETE);
		gm.addGroup("p_b", "[pb]'", TYPE.DELETE);
		gm.addGroup("dz_dZ", "(dz|dZ)'", TYPE.DELETE);
		gm.addGroup("G_x", "[Gx]'", TYPE.DELETE);
		gm.addGroup("v_f", "[vf]'", TYPE.DELETE);
		gm.addGroup("lmnr", "(\\^?[lmnr]\\.?)'", TYPE.DELETE);
		return gm;
	}
	
	public static GroupManager initGroup(XMLConfiguration conf, String activeManager) {
		GroupManager gm = new GroupManager();
		SubnodeConfiguration subConf = conf.configurationAt(C.GroupManager.getByIdXpath(activeManager));
		MatcherFactory.setMatcherClass(subConf.getString(C.GroupManager.matcher));
		List<HierarchicalConfiguration> hConfList = subConf.configurationsAt(C.Group.tag);
		for (HierarchicalConfiguration hConf : hConfList) {
			String groupName = hConf.getString(C.Group.name);
			String rule = hConf.getString(C.Group.rule);
			String params = hConf.getString(C.Group.params);
			if (params != null) {
				gm.addGroup(groupName, rule, params);
			} else {
				gm.addGroup(groupName, rule);
			}
		}
		return gm;
	}
	
	public static GroupManager initGroup(XMLConfiguration conf) {
		return initGroup(conf, conf.getString(C.activeGroupManager));
	}

}
