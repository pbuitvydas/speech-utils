package lt.vdu.speech.utils.mlfdiff.grouping;

import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;


public class MatcherFactory {
	
	private static String matcherClass = PhoneChangeMatcher.class.getName();
	
	public static Matcher create() {
		try {
			return (Matcher) Class.forName(matcherClass).newInstance();
		} catch (Exception e) {
			ExceptionHandler.getInstance().handle(e);
		}
		return null;
	}

	public static String getMatcherClass() {
		return matcherClass;
	}

	public static void setMatcherClass(String matcherClass) {
		MatcherFactory.matcherClass = matcherClass;
	}

}
