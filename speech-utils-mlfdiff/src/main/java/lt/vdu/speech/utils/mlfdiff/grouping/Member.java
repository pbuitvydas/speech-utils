package lt.vdu.speech.utils.mlfdiff.grouping;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Pakeitimu hitCountui sekimui, tikimybiu skaiciavimui
 * Map<String, Member> pakeitimai
 * s -> s, z, Z
 * @author pbuitvydas
 *
 */
public class Member {
	
	private static final Logger log = LoggerFactory.getLogger(Member.class);

	private Map<String, TransitionInfo> transitionMap = new HashMap<String, TransitionInfo>();
	
	
	public boolean addMember(String member) {
		if (transitionMap.containsKey(member)) {
			TransitionInfo info = transitionMap.get(member);
			info.setCount(info.getCount() + 1);
			log.info("Updating hit count for member: " + member + ", hit count is " + info.getCount());
			return true;
		}
		log.info("Adding new member: " + member);
		transitionMap.put(member, new TransitionInfo());
		return true;
	}


	public Map<String, TransitionInfo> getTransitionMap() {
		return transitionMap;
	}

}
