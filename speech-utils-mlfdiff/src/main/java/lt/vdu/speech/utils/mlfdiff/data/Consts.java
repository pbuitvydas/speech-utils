package lt.vdu.speech.utils.mlfdiff.data;

import java.util.ArrayList;
import java.util.List;

public class Consts {
	
	public static final List<String> STOP_PHONES = new ArrayList<String>() {
		{
			add("sill");
			add("sil");
			add(".");
			add("sp");
		}
	};

}
