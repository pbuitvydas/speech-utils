package lt.vdu.speech.utils.mlfdiff.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import difflib.Delta;

/**
 * 
 * @author pbuitvydas
 *
 */
public class TranscriptionHolder {
	
	private final Logger log = LoggerFactory.getLogger(TranscriptionHolder.class);
	
	private IndexHolder start; //pakeitimo pradzios indeksai
	private IndexHolder end; //pakeitimo pabaigos indeksai
	private Transcription orgTranscription; //originali transkripcija
	private Transcription revTranscription; //pakeista transkripcija
	private Delta delta;
	
	public double getScore() {
		return revTranscription.getScore() - orgTranscription.getScore();
	}
	
	public String getScoreAsString() {
		return String.format("%.6f", getScore());
	}
	
	@Override
	public String toString() {
		String retval = "";
		if (orgTranscription != null) {
			retval += "[orgtranscription: " + orgTranscription.toString() + "] ";
		}
		if (revTranscription != null) {
			retval += "[revtranscription: " + revTranscription.toString() + "]";
		}
		return retval;
	}
	
	protected TranscriptionHolder clone() {
		TranscriptionHolder holder = new TranscriptionHolder();
		holder.delta = delta;
		holder.end = new IndexHolder(end.getOrgIdx(), end.getRevIdx());
		holder.start = new IndexHolder(start.getOrgIdx(), start.getRevIdx());
		Transcription trO = new Transcription();
		trO.getTranscription().addAll(orgTranscription.getTranscription());
		holder.orgTranscription = trO;
		trO = new Transcription();
		trO.getTranscription().addAll(revTranscription.getTranscription());
		holder.revTranscription = trO;
		return holder;
	}

	public IndexHolder getStart() {
		return start;
	}

	public void setStart(IndexHolder start) {
		this.start = start;
	}

	public IndexHolder getEnd() {
		return end;
	}

	public void setEnd(IndexHolder end) {
		this.end = end;
	}

	public Transcription getOrgTranscription() {
		return orgTranscription;
	}

	public void setOrgTranscription(Transcription orgTranscription) {
		this.orgTranscription = orgTranscription;
	}

	public Transcription getRevTranscription() {
		return revTranscription;
	}

	public void setRevTranscription(Transcription revTranscription) {
		this.revTranscription = revTranscription;
	}

	public Delta getDelta() {
		return delta;
	}

	public void setDelta(Delta delta) {
		this.delta = delta;
	}
}
