package lt.vdu.speech.utils.mlfdiff.data;

/**
 * 
 * @author pbuitvydas
 *
 */
public class IndexHolder {
	private int orgIdx;
	private int revIdx;
	
	public IndexHolder(int orgIdx, int revIdx) {
		this.orgIdx = orgIdx;
		this.revIdx = revIdx;
	}
	
	@Override
	public String toString() {
		return "orgIdx=" + orgIdx + " revIdx=" + revIdx;
	}

	public int getOrgIdx() {
		return orgIdx;
	}

	public void setOrgIdx(int orgIdx) {
		this.orgIdx = orgIdx;
	}

	public int getRevIdx() {
		return revIdx;
	}

	public void setRevIdx(int revIdx) {
		this.revIdx = revIdx;
	}
	
	public void incOrg() {
		orgIdx++;
	}
	
	public void decOrg() {
		orgIdx--;
	}
	
	public void incRev() {
		revIdx++;
	}
	
	public void decRev() {
		revIdx--;
	}
}