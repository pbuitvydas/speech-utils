package lt.vdu.speech.utils.mlfdiff.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import difflib.Delta;
import difflib.Patch;

import lt.vdu.speech.utils.common.diff.BaseDiffWrapper;
import lt.vdu.speech.utils.common.dstructures.Mlf;
import lt.vdu.speech.utils.mlfdiff.data.Change;
import lt.vdu.speech.utils.mlfdiff.data.IndexHolder;
import lt.vdu.speech.utils.mlfdiff.data.Transcription;
import lt.vdu.speech.utils.mlfdiff.data.TranscriptionHolder;

public abstract class BaseMlfWrapper extends BaseDiffWrapper<Mlf> {
	
	private static final Logger log = LoggerFactory.getLogger(BaseMlfWrapper.class);
	
	public String wrapBegin(Patch patch) {
		return null;
	}
	
	public String wrap(Delta delta) {
		return null;
	}
	
	public String wrapEnd(Patch patch) {
		return null;
	}
	
    public String getLabName(List<Mlf> list, int position) {
    	return list.get(position).getLabName();
    }

	public double getScore(Mlf item) {
		float secs = item.getStop2() - item.getStart2();
		secs *= item.getProba();
		return secs;
	}
	
	/**
	 * pagal nurodytas pakeitimo pozicijas grazina originalo ir pakeisto transkripcijas, nuo pakeitimo į viršų ir į apačią vykdoma stabilaus intervalo paieška
	 * @param original - nuskaitytas originalus recout'as
	 * @param originalPosition - pakeitimo pozicija origale
	 * @param revised - nuskaitytas pakeistas recout'as
	 * @param revisedPosition - pakeitimo pozicija modifikuotame
	 * @return originalo bei pakeisto intervalų transkripcijos
	 */
	public TranscriptionHolder getTraanscription(List<Mlf> original, List<Mlf> revised, Delta delta) {
		int originalPosition = delta.getOriginal().getPosition();
		int revisedPosition = delta.getRevised().getPosition();
		
		log.trace("Getting transcription for {}", getOriginal(originalPosition).getLabName());
		
		IndexHolder start = searchStableUp(originalPosition, revisedPosition);
		
		originalPosition += getPositionOffset(delta.getOriginal().getLines());
		revisedPosition += getPositionOffset(delta.getRevised().getLines());
		IndexHolder end = searchStableDown(originalPosition, revisedPosition);
 		TranscriptionHolder holder = new TranscriptionHolder();
 		holder.setDelta(delta);
 		holder.setStart(start);
 		holder.setEnd(end);
		if (start != null && end != null) {
			List<Change> transList = new ArrayList<Change>();
			Transcription tr = new Transcription();
			tr.setLabName(getLabName(revised, revisedPosition));
			for (int i = start.getRevIdx(); i <= end.getRevIdx(); i++) {
				Mlf mlf = getRevised(i);
				transList.add(new Change(mlf, null));
				tr.setScore(getScore(mlf) + tr.getScore());
			}
			tr.setTranscription(transList);
			updateScore(tr);
			holder.setRevTranscription(tr);
			
			List<Change> transListOrg = new ArrayList<Change>();
			Transcription trOrg = new Transcription();
			trOrg.setLabName(getLabName(original, originalPosition));
			for (int i = start.getOrgIdx(); i <= end.getOrgIdx(); i++) {
				Mlf mlf = getOriginal(i);
				transListOrg.add(new Change(mlf, null));
				trOrg.setScore(getScore(mlf) + trOrg.getScore());
			}
			trOrg.setTranscription(transListOrg);
			updateScore(trOrg);
			holder.setOrgTranscription(trOrg);
		}
		
		log.debug("Resulting transcription: {}", holder);
		return holder;
	}
	
	public int getPositionOffset(List<?> lines) {
		if (lines.size() > 0) {
			return lines.size() - 1;
		}
		return 0;
	}
	
	/**
	 * atnaujinamas išlošimas: rezultatas padalinamas iš intervalo ilgio
	 * @param tr
	 */
	public void updateScore(Transcription tr) {
		if (tr.getTranscription().size() == 0) {
			return;
		}
 		Change first = tr.getTranscription().get(0);
		Change last = tr.getTranscription().get(tr.getTranscription().size()-1);
		long totalTime = last.getMlf().getStop2() - first.getMlf().getStart2();
		tr.setScore(tr.getScore() / totalTime);
	}
	
	public IndexHolder searchStableUp(int originalPosition, int revisedPosition) {
		IndexHolder indexes = searchStableUp(getOriginal(), originalPosition, getRevised(), revisedPosition);
		return indexes;
	}
	
	public IndexHolder searchStableUp(List<Mlf> original, int originalPosition, List<Mlf> revised, int revisedPosition) {

		//??? TODO ar veikia ar reikalingas?
		if (isStableChange(originalPosition, revisedPosition)) {
			return new IndexHolder(originalPosition, revisedPosition);
		}
		Mlf org = null, rev = null;
		do {
			IndexHolder indexes = synchronizePositionsUp(originalPosition, revisedPosition);
			originalPosition = indexes.getOrgIdx();
			revisedPosition = indexes.getRevIdx();
			org = getOriginal(originalPosition);
			rev = getRevised(revisedPosition);
			if (org != null && rev != null) {
				if (isStableStart(originalPosition, revisedPosition)) {
					log.trace("Found stable start at [orgMlf: {}] [revMlf: {}]", getOriginal(originalPosition+1), getRevised(revisedPosition+1));
					return new IndexHolder(originalPosition+1, revisedPosition+1);
				}
				originalPosition--;
				revisedPosition--;
			}
		} while (org != null && rev != null );
		//TODO log error
		return null;
	}
	
	public IndexHolder synchronizePositionsUp(int originalPosition, int revisedPosition) {
		Mlf org = getOriginal(originalPosition);
		Mlf rev = getRevised(revisedPosition);
		while (org != null && rev != null && !org.getItem().equals(rev.getItem())) {
			if (org.getStop2() > rev.getStop2() && org.getStart2() != 0) {
				originalPosition--;
			} else if (org.getStop2() - org.getStart2() == 0) {
				originalPosition--;
			} else if (rev.getStop2() - rev.getStart2() == 0) {
				revisedPosition--;
			} else {
				revisedPosition--;
			}
			org = getOriginal(originalPosition);
			rev = getRevised(revisedPosition);
		}
		log.trace("Synchronising indexes up at [orgMlf: {}] [revMlf: {}]", getOriginal(originalPosition), getRevised(revisedPosition));
		return new IndexHolder(originalPosition, revisedPosition);
	}
	
	public boolean isStableStart(int originalPos, int revisedPos) {
		return isSameItems(originalPos, revisedPos);
	}
	
	/**
	 * ptikrina ar stabili pabaiga, turi tenkinti tokias salygas:<br/>
	 * 1 nesutampa prieš pakeitimą einantis intervalas <br/>
	 * 2 sutampa pateiktoje pozicijoje intervalų pabaigos <br/>
	 * 3 sutampa sekantis intervalas <br/>
	 * @param originalPos
	 * @param revisedPos
	 * @return
	 */
	public boolean isStableEnd(int originalPos, int revisedPos) {
		return isSameItems(originalPos, revisedPos);
	}
	
	
	public IndexHolder searchStableDown(int originalPosition, int revisedPosition) {
		return searchStableDown(getOriginal(), originalPosition, getRevised(), revisedPosition);
	}
	
	public IndexHolder searchStableDown(List<Mlf> original, int originalPosition, List<Mlf> revised, int revisedPosition) {

		if (isStableChange(originalPosition, revisedPosition)) {
			return new IndexHolder(originalPosition, revisedPosition);
		}
		Mlf org = null, rev = null;
		do {
			IndexHolder indexes = synchronizePositionsDown(originalPosition, revisedPosition);
			originalPosition = indexes.getOrgIdx();
			revisedPosition = indexes.getRevIdx();
			org = getOriginal(originalPosition);
			rev = getRevised(revisedPosition);
			if (org != null && rev != null) {
				if (isStableEnd(originalPosition, revisedPosition)) {
					log.trace("Found stable end at [orgMlf: {}] [revMlf: {}]", getOriginal(originalPosition-1), getRevised(revisedPosition-1));
					return new IndexHolder(originalPosition-1, revisedPosition-1);					
				}
				originalPosition++;
				revisedPosition++;
			}
		} while (org != null && rev != null );
		return null;
	}
	
	public IndexHolder chooseOffsetDown(int originalPosition, int revisedPosition) {
		Mlf org = getOriginal(revisedPosition);
		Mlf rev = getRevised(revisedPosition);
		if (org == null || rev == null) {
			return null;
		}
		Mlf orgNext = getOriginal(revisedPosition + 1);
		Mlf revNext = getRevised(revisedPosition + 1);
		if (orgNext == null || revNext == null) {
			return null;
		}
		
		long dOrg = orgNext.getStop2() - org.getStop2();
		long dRev = revNext.getStop2() - rev.getStop2();
		if (dOrg < dRev) {
			++originalPosition;
		} else {
			++revisedPosition;
		}
		return new IndexHolder(originalPosition, revisedPosition);
		
	}
	
	/**
	 * susinchronizuoja pakeitimų intervaluis taip kad modifikuotas stovėtų virš arba sulig originaliu
	 * @param originalPosition
	 * @param revisedPosition
	 * @return
	 */
	public IndexHolder synchronizePositionsDown(int originalPosition, int revisedPosition) {
		Mlf org = getOriginal(originalPosition);
		Mlf rev = getRevised(revisedPosition);
		while (org != null && rev != null && !org.getItem().equals(rev.getItem())) {
			if (org.getStop2() < rev.getStop2()) {
				originalPosition++;
			} else if (org.getStop2() - org.getStart2() == 0) {
				originalPosition++;
			} else if (rev.getStop2() - rev.getStart2() == 0) {
				revisedPosition++;
			} else {
				revisedPosition++;
			}
			org = getOriginal(originalPosition);
			rev = getRevised(revisedPosition);
		}
		log.trace("Synchronising indexes down at [orgMlf: {}] [revMlf: {}]", getOriginal(originalPosition), getRevised(revisedPosition));
		return new IndexHolder(originalPosition, revisedPosition);
	}
	
	/**
	 * patikrina ar sekančio dvi intervalų poros sutampa
	 * @param originalPosition
	 * @param revisedPosition
	 * @return
	 */
	public boolean isTwoNextIntervalsSame(int originalPosition, int revisedPosition) {
		return isSameItems(originalPosition + 1, revisedPosition + 1) 
				&& isSameItems(originalPosition + 2, revisedPosition + 2);
	}
	
	/**
	 * patikrina ar nurodytose pozicijose pakeitimas iskarto yra stabilus
	 * t.y. ar pakeitimo ir jo kaimynų intervalai sutampa
	 * @param originalPos
	 * @param revisedPos
	 * @return
	 */
	public boolean isStableChange(int originalPos, int revisedPos) {
		return isSameItems(originalPos, revisedPos) 
				&& isSameItems(originalPos -1, revisedPos -1)
				&& isSameItems(originalPos + 1, revisedPos + 1);
	}
	
	/**
	 * pritikrina ar nurodytose pozicijose pakeitimo intervalas yra vienodas
	 * @param originalPos - originalaus failo pakeitimo pozicija
	 * @param revisedPos - modifikuoto failo pakeitimo pozicija
	 * @return
	 */
	public boolean isSameItems(int originalPos, int revisedPos) {
		Mlf original = getOriginal(originalPos);
		Mlf revised = getRevised(revisedPos);
		return isSameItems(original, revised);
	}
	
	public boolean isSameScore(Mlf original, Mlf revised) {
		if (original == null || revised == null) {
			return false;
		}
		return original.getProba() == revised.getProba();
		
	}
	
	public boolean isSameStop(Mlf original, Mlf revised) {
		if (original == null || revised == null) {
			return false;
		}
		return original.getStop2() == revised.getStop2();
	}
	
	public boolean isSameStart(Mlf original, Mlf revised) {
		if (original == null || revised == null) {
			return false;
		}
		return original.getStart2() == revised.getStart2();
	}
	
	public boolean isSameItems(Mlf original, Mlf revised) {
		if (original == null || revised == null) {
			return false;
		}
		return original.getStart2() == revised.getStart2() && original.getStop2() == revised.getStop2()
				&& original.getProba() == revised.getProba()
				&& original.getItem().equals(revised.getItem());
	}

}
