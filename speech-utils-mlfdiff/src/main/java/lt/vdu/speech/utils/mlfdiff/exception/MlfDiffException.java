package lt.vdu.speech.utils.mlfdiff.exception;

import lt.vdu.speech.utils.common.exception.SpeechUtilsException;

public class MlfDiffException extends SpeechUtilsException {

	private static final long serialVersionUID = 1630766650551159736L;

	public MlfDiffException(String description) {
		super(description);
	}

}
