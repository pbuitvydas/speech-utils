package lt.vdu.speech.utils.mlfdiff.grouping;

import lt.vdu.speech.utils.mlfdiff.data.TranscriptionHolder;

public interface Matcher {
	
	void setup(String matchRule, Object ...params);

	boolean matches(TranscriptionHolder holder);

}
