package lt.vdu.speech.utils.mlfdiff.wrapper;

public interface WrapperListener {
	void onWrapBegin();
	void onWrap(Object data);
	void onWrapEnd();
}
