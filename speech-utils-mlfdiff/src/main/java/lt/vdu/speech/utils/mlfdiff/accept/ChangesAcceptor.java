package lt.vdu.speech.utils.mlfdiff.accept;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lt.vdu.speech.utils.common.diff.CommonDiff;
import lt.vdu.speech.utils.common.dstructures.Mlf;
import lt.vdu.speech.utils.common.io.writer.BaseWriter;
import lt.vdu.speech.utils.common.io.writer.BasicBufferedWriter;
import lt.vdu.speech.utils.mlfdiff.data.TranscriptionHolder;
import lt.vdu.speech.utils.mlfdiff.wrapper.textgrid.TextGridwrapper;
import difflib.Delta;

public class ChangesAcceptor extends TextGridwrapper {
	
	private static final Logger log = LoggerFactory.getLogger(ChangesAcceptor.class);
	
	private double epsilon;
	private String outputFile;
	
	public ChangesAcceptor(boolean acceptAll, String outputFile) {
		this(acceptAll ? Double.NEGATIVE_INFINITY : Double.POSITIVE_INFINITY, outputFile);
	}
	
	public ChangesAcceptor(double epsilon, String outputFile) {
		this.epsilon = epsilon;
		this.outputFile = outputFile;
	}
	
	public void acceptRejectChanges(CommonDiff<Mlf> differ) {
		setOriginal(differ.getOriginal());
		setRevised(differ.getRevised());
		
		List<Delta> deltaList = differ.getPatch().getDeltas();
		List<Delta> tempList = new ArrayList<Delta>();
		
		log.info("Accepting changes, total of {} changes found", deltaList.size());
		for (int i = 0; i < deltaList.size(); i++) {
			Delta d = deltaList.get(i);
			if (isAcceptableChange(d)) {
				tempList.add(d);
			}
		}
		deltaList.clear();
		deltaList.addAll(tempList);
		writeResults(differ);
		log.info("Done accepting changes, rejected changes: {}", deltaList.size() - tempList.size());
	}
	
	public void writeResults(CommonDiff<Mlf> differ) {
		
		log.info("Patching original changes...");
		
		List<Mlf> patchedList = differ.patch();
		
		log.info("Writing result to file {}", outputFile);
		
		BasicBufferedWriter<Mlf> writer = new BasicBufferedWriter<Mlf>(outputFile);
		writer.write(patchedList);
		writer.close();
		
	}
	
	
	@Override
	protected void wrapBody(BaseWriter<?, ?> writer, List<Delta> deltaList) {
		
	}
	
	
	protected boolean isAcceptableChange(Delta delta) {
		TranscriptionHolder holder = getTraanscription(delta);
		log.trace("Analysing transcription {}", holder);
		if (holder.getScore() >= epsilon) {
			return true;
		}
		log.trace("Found rejectable change: {} < {}, {}", epsilon, holder.getScore(), holder);
		return false;
	}

}
