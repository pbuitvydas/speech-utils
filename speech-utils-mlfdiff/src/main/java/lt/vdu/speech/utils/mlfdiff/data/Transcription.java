package lt.vdu.speech.utils.mlfdiff.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import lt.vdu.speech.utils.common.dstructures.Mlf;

public class Transcription {
	private List<Change> transcriptionList = new ArrayList<Change>();
	private String labName;
	private double score;
	
	public List<Change> getTranscription() {
		return transcriptionList;
	}
	public void setTranscription(List<Change> transcription) {
		this.transcriptionList = transcription;
	}
	public String getLabName() {
		return labName;
	}
	public void setLabName(String labName) {
		this.labName = labName;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	
	public Long getStart() {
		if (transcriptionList.size() > 0) {
			Mlf mlf = transcriptionList.get(0).getMlf();
			if (mlf != null) {
				return mlf.getStart2();
			}
		}
		return null;
	}
	
	public Long getStop() {
		if (transcriptionList.size() > 0) {
			Mlf mlf = transcriptionList.get(transcriptionList.size()-1).getMlf();
			if (mlf != null)
			return mlf.getStop2();
		}
		return null;
	}
	
	//TODO sulipdyt iskarto
	public String transcriptionToString() {
		String retVal = "";
		Iterator<Change> it = transcriptionList.iterator();
		while (it.hasNext()) {
			retVal += it.next().getMlf().getItem();
			if (it.hasNext()) {
				retVal += " ";
			}
		}
		return retVal;
	}
	
	/**
	 * naudojamas atspausdinti objekto busena, norint gauti tik transkrpcija naudoti transcriptionToString()
	 */
	public String toString() {
		return transcriptionToString() + " " + score;
	}
	
	public int indexOfTeeModel() {
		for (int i = 0; i < transcriptionList.size(); i++) {
			Change chg = transcriptionList.get(i);
			if (Consts.STOP_PHONES.contains(chg.getMlf().getItem())) {
				return i;
			}
		}
		return -1;
	}
	
	public boolean containsModel(String model) {
		for (Change ch : transcriptionList) {
			if (model.equals(ch.getMlf().getItem())) {
				return true;
			}
		}
		return false;
	}
	
}
