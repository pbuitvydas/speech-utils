package lt.vdu.speech.utils.mlfdiff.data;

import lt.vdu.speech.utils.common.dstructures.Mlf;
import difflib.Delta.TYPE;

public class Change {
	private Mlf change;
	private TYPE type;
	
	public Change(Mlf change, TYPE type) {
		this.change = change;
		this.type = type;
	}
	
	public Mlf getMlf() {
		return change;
	}
	
	public TYPE getType() {
		return type;
	}
}
