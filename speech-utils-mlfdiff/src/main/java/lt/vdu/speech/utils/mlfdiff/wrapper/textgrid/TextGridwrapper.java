package lt.vdu.speech.utils.mlfdiff.wrapper.textgrid;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lt.vdu.speech.utils.common.dstructures.Mlf;
import lt.vdu.speech.utils.common.dstructures.textgrid.Description;
import lt.vdu.speech.utils.common.dstructures.textgrid.Interval;
import lt.vdu.speech.utils.common.dstructures.textgrid.Item;
import lt.vdu.speech.utils.common.dstructures.textgrid.Range;
import lt.vdu.speech.utils.common.dstructures.textgrid.TextGrid;
import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;
import lt.vdu.speech.utils.common.global.Context;
import lt.vdu.speech.utils.common.io.writer.BaseWriter;
import lt.vdu.speech.utils.common.io.writer.TextGridWriter;
import lt.vdu.speech.utils.common.utilities.RangeUtils;
import lt.vdu.speech.utils.mlfdiff.data.Change;
import lt.vdu.speech.utils.mlfdiff.data.Transcription;
import lt.vdu.speech.utils.mlfdiff.data.TranscriptionHolder;
import lt.vdu.speech.utils.mlfdiff.grouping.GroupManager;
import lt.vdu.speech.utils.mlfdiff.grouping.GroupMember;
import lt.vdu.speech.utils.mlfdiff.wrapper.BaseMlfWrapper;
import lt.vdu.speech.utils.mlfdiff.wrapper.WrapperListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import difflib.Delta;
import difflib.Delta.TYPE;
import difflib.Patch;

//TODO iskilnoto vidines klases atskirai
//TODO enkapsuliacija !!!! Range, TranscriptionHolderyje ir tt
public class TextGridwrapper extends BaseMlfWrapper {
	
	public static final String CHANGES_TIER_NAME = "pakeitimai";
	public static final int MAX_SEARCH = 2;
	private GroupManager groupManager;
	private List<WrapperListener> listenerList = new ArrayList<WrapperListener>();
	
	private static final Logger log = LoggerFactory.getLogger(TextGridwrapper.class);
	
	{
		//FIXME nesamone cia per konstruktorius gerriau
		groupManager = (GroupManager) Context.getInsTance().getGlobal();
	}
	
	public TextGridwrapper() {
		listenerList.add(new DissapearedSpSkipSlistener());
		listenerList.add(new NanScorePrepocessListener());
		listenerList.add(new TranscriptionExpandDeleteOnlyListener());
		listenerList.add(new ZerosScoreProcessListener());
		//listenerList.add(new DebugListener());
	}
	
	@Override
	public void wrapBegin(BaseWriter<?,?> writer, Patch patch) {
		
	}
	
	@Override
	public void wrapEnd(BaseWriter<?, ?> writer, Patch patch) {
	}
	
	/*
	 * Grupiu suradima reikia fixint
	 * Problemos kai suminkstejimas papuola
	 * Pergalvot pagal ka spresti i ka papuola rev ar org ar kombinuotai
	 * Grupem prideti kuriem veiksmam taikyt pvz minkst grupei tik TYTPE.Change
	 * visom kitom Type.Change, Type.Delete
	 * pvz (String groupName, String pattern, Type ...types)
	 * Reiktu duoti spresti pagal deltos duomenis???
	 * (non-Javadoc)
	 * @see lt.vdu.speech.utils.common.diff.BaseDiffWrapper#wrapBody(lt.vdu.speech.utils.common.io.writer.BaseWriter, java.util.List)
	 */
	@Override
	protected void wrapBody(BaseWriter<?, ?> writer, List<Delta> deltaList) {
		groupManager.resetMembersIndexes();
		notifyOnWrapBegin();
		//sudedama grupių indeksai
		List<TranscriptionHolder> holderList = new ArrayList<TranscriptionHolder>();
		log.info("processing: {}", Context.getInsTance().getSpeaker());
		log.info("total diferences found: {}", deltaList.size());
    	for (int i = 0; i < deltaList.size(); i++) {
    		Delta d = deltaList.get(i);
    		TranscriptionHolder holder = getTraanscription(d);
    		holderList.add(holder);
    		groupManager.updateGroupPositions(holder, i);
    	}
    	for (GroupMember member : groupManager.getGroupList()) {//atliekamas textgridu grupavimas
    		wrapBody(holderList, member);
    	}

    	notifyOnWrapEnd();
	}
	
	/**
	 * į juostą įdeda intervalą patikrina egzistavimą juostų, atlieka triminimą intervalų, persidengimą patrikrina
	 * @param juosta
	 * @param interval
	 */
	public void addInterval(Item juosta, Interval interval) {
		log.debug("Adding interval: {}", interval);
		if (isRangeExists(juosta, interval.range)) {
			log.debug("Range exists skipping...");
			return;
		}
		if (juosta.intervals.size() == 0 && interval.range.xmin != 0) {
			juosta.addInterval(createDummyInterval(0, interval.range.xmin));
		}
		if (juosta.intervals.size() != 0) {
			Interval lastInterval = juosta.intervals.get(juosta.intervals.size() -1);
			if (lastInterval.range.xmax != interval.range.xmin) {
				if (lastInterval.range.xmax > interval.range.xmin) { //persidengimas
					log.debug("Overlap occured ({} > {}), trimming start of interval", lastInterval.range.xmax, interval.range.xmin);
					interval.range.xmin = lastInterval.range.xmax;
				} else {
					Interval dumyy = createDummyInterval(lastInterval.range.xmax, interval.range.xmin);
					juosta.addInterval(dumyy);
				}
			}
		}
		juosta.addInterval(interval);
	}

	protected void wrapBody(List<TranscriptionHolder> deltaList, GroupMember group) {
		String dir = Context.getInsTance().getOutputDir().getAbsolutePath();
		String start = dir.substring(0, dir.lastIndexOf('\\'));
		String end = dir.substring(dir.lastIndexOf('\\'));
		dir = start + "\\" + group.getGroupName() + end;
		String previousLab = null;
		String labName = null;
		TextGrid tg = null;
		Item juosta = null;
		log.info("processing group={} size={}", group.getGroupName(), group.getIndexList().size());
		for (int i = 0; i < group.getIndexList().size(); i++) {
			TranscriptionHolder holder = deltaList.get(group.getIndexList().get(i));
    		if (holder.getOrgTranscription().getStart() == null) {
    			log.error("generated transcription has no start time");
    			log.error("{}:", getOriginal(holder.getDelta().getOriginal().getPosition()).getLabName());
    			log.error("original lines: {}", holder.getDelta().getOriginal().getLines());
    			log.error("revised  lines: {}", holder.getDelta().getRevised().getLines());
    			continue;
    		}
			labName = getOriginal(holder.getDelta().getOriginal().getPosition()).getLabName();
			if (labName == null) {
				log.error("!!! Lab name is null, previous lab was {}", previousLab);
				break;
			}
			if (!labName.equals(previousLab)) {
				writeGrid(tg, previousLab, dir);
				
				log.trace("labRange is: " + getActualRange(labName));
				log.trace("Creating new grid for lab: {}", labName);
				tg = initTextGrid(labName);
				juosta = tg.getItem(CHANGES_TIER_NAME); 
			}

			if (holder.getRevTranscription() != null) {
				log.trace("Lab name={}", labName);
				log.trace("original transcription: {} {} {}", holder.getOrgTranscription(), holder.getOrgTranscription().getStart(), holder.getOrgTranscription().getStop());
				log.trace("revised transcription: {} {} {}", holder.getRevTranscription(), holder.getRevTranscription().getStart(), holder.getRevTranscription().getStop());
				
				Interval interval = getInterval(holder);
				addInterval(juosta, interval);
			} else {
				log.error("!!! revised transcription is null");
			}
			
			previousLab = labName;
		}
		writeGrid(tg, previousLab, dir);
		createEmptyGrids(dir);
	}

	/**
	 * įrašo suformuotą texgridą į failą
	 * @param grid
	 * @param labName
	 * @param dir
	 */
	public void writeGrid(TextGrid grid, String labName, String dir) {
		if (grid == null) {
			return;
		}
		log.info("Writing created grid for: {}", labName);
		labName = labName.replace(".lab", ".TextGrid");
		File f = new File(dir);
		if (!f.exists()) {
			f.mkdirs();
		}
		ensureProperEnd(grid);
		TextGridWriter writer = new TextGridWriter(dir + "/" + labName);
		try {
			writer.writeData(grid);
			writer.close();
		} catch (IOException e) {
			ExceptionHandler.getInstance().handle(e);
		}
	}
	
	/**
	 * užtikrina kad juostos memberis užsibaigtų tokiu laiku koks nurodytas juostos intervale
	 * @param tg
	 */
	public void ensureProperEnd(TextGrid tg) {
		Item juosta = tg.getItem(CHANGES_TIER_NAME);
		Interval last = juosta.intervals.size() > 0 ? juosta.intervals.get(juosta.intervals.size()-1) : null;
		if (last.range.xmax != juosta.range.xmax) {
			juosta.addInterval(createDummyInterval(last.range.xmax, juosta.range.xmax));
		} else if (last.range.xmax > juosta.range.xmax) {
			log.error("!!! pabaigos intervalas virsija juostos pabaiga");
			//TODO throw exception
		}
	}
	
	/**
	 * sukuria tuščią intervalą
	 * @param xmin
	 * @param xmax
	 * @return
	 */
	public Interval createDummyInterval(float xmin, float xmax) {
		Interval dummy = new Interval();
		dummy.text = "";
		dummy.range = new Range(xmin, xmax);
		return dummy;
	}
	
	/**
	 * perduotiems pradiniam ir pakeistam transkripcijom paskaičiuoja išlošimą ir sukuria intervalą kuriame įvyko pakeitimas
	 * naudojamas formuojant textgrido juostos itemą
	 * @param orgTranscription
	 * @param revTranascription
	 * @return
	 */
	public Interval getInterval(TranscriptionHolder holder) {
		Transcription orgTranscription = holder.getOrgTranscription();
		Interval interval = new Interval();
		interval.text = holder.getScoreAsString();
		interval.range = RangeUtils.rangeFromFrames(orgTranscription.getStart(), orgTranscription.getStop());
		return interval;
	}
	
	/**
	 * patikrina ar juostoje jau yra toks intrevalas
	 * @param juosta
	 * @param range
	 * @return
	 */
	public boolean isRangeExists(Item juosta, Range range) {
		for (Interval interval : juosta.intervals) {
			if (interval.range.xmin >= range.xmin && interval.range.xmax <= range.xmax) {
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * sukuria pradinį tuščią grid'ą
	 * @param labName
	 * @return
	 */
	public TextGrid initTextGrid(String labName) {
		TextGrid tg = new TextGrid();
		tg.range = getActualRange(labName);
		tg.description = new Description();
		Item itm = new Item(); //nauja juosta
		itm.range = getActualRange(labName);
		itm.name = CHANGES_TIER_NAME;
		itm.claz = "IntervalTier"; //default
		tg.addItem(itm); //idedama juosta
		return tg;
	}
	
	/**
	 * sukuria tuščius pakeitimų gridus, jei kazkuriam lab'e nebuvo pakeitimų bu sukuriamas atitinkamas gridas, su vienu intervalu <--> sakinio ilgiu 
	 * @deprecated - šalinti šį funkcionalumą arba iškelti į listenerius
	 * @param dir
	 */
	@Deprecated
	public void createEmptyGrids(String dir) {
		log.info("checking for missing grids...");
		if (dir.contains("SIR")) { //TODO remove hardcode
			for (int i = 1; i < 80; i++) {
				checkAndCreateIfNecessary(dir, i);
			}
		} else {
			for (int i = 1; i <= 30; i++) {
				checkAndCreateIfNecessary(dir, i);
			}
		}
	}
	
	/**
	 * 
	 * @param dir
	 * @param i
	 */
	public void checkAndCreateIfNecessary(String dir, int i) {
		String tgFn = formTextGridFn(i);
		String lab = tgFn.replace(".TextGrid", ".lab");
		File f = new File(dir + "\\" + tgFn);
		if (!f.exists()) {
			log.trace("{} not found creating empty in dir: {}", tgFn, dir);
			TextGrid tg = createEmptyTextGrid(lab);
			writeGrid(tg, lab, dir);
		}
	}
	
	
	public String formTextGridFn(int i) {
		return String.format("%s_A%03d.TextGrid", Context.getInsTance().getSpeaker(), i);
	}
	
	public TextGrid createEmptyTextGrid(String labName) {
		TextGrid tg = initTextGrid(labName);
		Item juosta = tg.getItem(CHANGES_TIER_NAME);
		juosta.addInterval(createDummyInterval(juosta.range.xmin, juosta.range.xmax));
		return tg;
	}
	
	/**
	 * pagal nurodytą lab'ą suranda jo intervalą tikrinimas vykdomas perrenkant nuo .lab iki .
	 * @param labName
	 * @return
	 */
	public Range getActualRange(String labName) {
		for (int i = 0; i < getOriginal().size(); i++) {
			Mlf mlf = getOriginal().get(i);
			if (mlf.getLeft() == null && mlf.getItem().contains(labName)) {
				for (int j = i; j < getOriginal().size(); j++) {
					mlf = getOriginal().get(j);
					if (mlf.getItem().equals(".")) {
						mlf = getOriginal().get(j-1);
						return RangeUtils.rangeFromFrames(0, mlf.getStop2());
					}
				}
			}
		}
		Mlf mlf = getOriginal().get(getOriginal().size()-2);//nes buna mlf'e gale . kaip transkripcija
		return RangeUtils.rangeFromFrames(0, mlf.getStop2());
	}
	
	public TranscriptionHolder getTraanscription(Delta delta) {
		TranscriptionHolder holder = getTraanscription(getOriginal(), getRevised(), delta);
		if (holder.toString().contains("sil v ^o:")) {
			holder = getTraanscription(getOriginal(), getRevised(), delta);
		}
		notifyOnWrap(holder);
		return holder;
	}
	
	public void addWrapperListener(WrapperListener wrapperListener) {
		listenerList.add(wrapperListener);
	}
	
	public void clearWrapperListeners() {
		listenerList.clear();
	}
	
	public void notifyOnWrapBegin() {
		for (WrapperListener wrapperListener : listenerList) {
			wrapperListener.onWrapBegin();
		}
	}
	
	public void notifyOnWrap(Object data) {
		for (WrapperListener wrapperListener : listenerList) {
			wrapperListener.onWrap(data);
		}
	}
	
	public void notifyOnWrapEnd() {
		for (WrapperListener wrapperListener : listenerList) {
			wrapperListener.onWrapEnd();
		}
	}
	
	/**
	 * @deprecated šalint
	 */
	@Deprecated
	public void swapListeners() {
		clearWrapperListeners();
		addWrapperListener(new NanScorePrepocessListener());
		addWrapperListener(new TranscriptionExpandDeleteOnlyListener());
		//addWrapperListener(new DebugListener());
	}
	
	public class HolderCmpByScore implements Comparator<TranscriptionHolder> {

		@Override
		public int compare(TranscriptionHolder o1, TranscriptionHolder o2) {
			double scoreLeft = o1.getRevTranscription().getScore() - o1.getOrgTranscription().getScore();
			double scoreRight = o2.getRevTranscription().getScore() - o2.getOrgTranscription().getScore();
			double result = scoreLeft - scoreRight;
			if (Double.isNaN(result)) {
				return 0;
			}
			if (result < 0) {
				return -1;
			}
			if (result > 0) {
				return 1;
			}
			return 0;
		}
		
	}
	
	public class HolderCmpBySize implements Comparator<TranscriptionHolder> {

		@Override
		public int compare(TranscriptionHolder o1, TranscriptionHolder o2) {
			return o2.getOrgTranscription().getTranscription().size() - o1.getOrgTranscription().getTranscription().size();
		}
		
	}
	
	/*
	 * prapl4timo bugas, kai sp gale pvz u s ps i sp, vaizdavima gali tokio nerodyt
	 */
	public class TranscripcionExpandListener implements WrapperListener {

		@Override
		public void onWrapBegin() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onWrap(Object data) {
			if (data instanceof TranscriptionHolder) {
				expandTranscription((TranscriptionHolder) data);
			}
			
		}

		@Override
		public void onWrapEnd() {
			// TODO Auto-generated method stub
			
		}
		
		/**
		 * praplečia transkripcijų intervalus, iki 5 vienetų papildomas ? sp ? papildomas
		 * @param holder
		 */
		protected void expandTranscription(TranscriptionHolder holder) {
			if (holder == null) {
				return;
			}
			if (holder.getOrgTranscription() == null || holder.getRevTranscription() == null) {
				return;
			}
			if (holder.getScore() == 0) {//jei score = 0 neidomus atvejis net nereikia praplėsti??
				return;
			}
			int indexOfTee = holder.getOrgTranscription().indexOfTeeModel();
			if (indexOfTee != -1) {
				if (holder.getOrgTranscription().getTranscription().size() < 5) {
					int addToFront = 2 - indexOfTee;
					addToFront(addToFront, holder);
					int addToback = 5 - holder.getOrgTranscription().getTranscription().size();
					addToBack(addToback, holder);
				}
			} else {
				//pabandyt surast sp;
				Mlf rec = getOriginal(holder.getEnd().getOrgIdx() + 1);
				Mlf rev = getRevised(holder.getEnd().getRevIdx()+1);
				if (rec != null && "sp".equals(rec.getItem())) {
					holder.getEnd().incOrg();
					holder.getOrgTranscription().getTranscription().add(new Change(rec, null));
					if (rev != null && "sp".equals(rev.getItem())) {
						holder.getEnd().incRev();
						holder.getRevTranscription().getTranscription().add(new Change(rec, null));
					}
					expandTranscription(holder);
				}
			}
		}
		
		/**
		 * papildo pradžioje
		 * @param count kiek papildyti
		 * @param holder kur papildyti
		 */
		protected void addToFront(int count, TranscriptionHolder holder) {
			for (int i = 0; i < count; i++) {
				Integer orgIdx = holder.getStart().getOrgIdx();
				Integer revIdx = holder.getStart().getRevIdx();
				Mlf original = getOriginal(--orgIdx);
				Mlf revised = getRevised(--revIdx);
				if (original != null && revised != null) {
					holder.getOrgTranscription().getTranscription().add(0, new Change(original, null));
					holder.getRevTranscription().getTranscription().add(0, new Change(revised, null));
				}
			}
		}
		
		/**
		 * papildyti gale 
		 * @param count kiek papildyti
		 * @param holder kur papildyti
		 */
		protected void addToBack(int count, TranscriptionHolder holder) {
			for (int i = 0; i < count; i++) {
				Integer orgIdx = holder.getEnd().getOrgIdx();
				Integer revIdx = holder.getEnd().getRevIdx();
				Mlf original = getOriginal(++orgIdx);
				Mlf revised = getRevised(++revIdx);
				if (original != null && revised != null) {
					holder.getOrgTranscription().getTranscription().add(new Change(original, null));
					holder.getRevTranscription().getTranscription().add(new Change(revised, null));
				}
			}
		}
		
		protected void expandFront(TranscriptionHolder holder) {
			
		}
		
		protected void expandBack(TranscriptionHolder holder) {
			
		}
		
		protected int getPos(Transcription transcription, Mlf recout) {
			for (int i = 0; i < transcription.getTranscription().size(); i++) {
				Change chg = transcription.getTranscription().get(i);
				if (chg.getMlf().equalsByFields(recout)) {
					return i;
				}
			}
			return -1;
		}
		
	}
	
	public class DebugListener implements WrapperListener {
		
		private static final String DEBUG_DIR = "d:/debug";
		
		private Map<String, TranscriptionHolder> map = new HashMap<String, TranscriptionHolder>();
		private List<TranscriptionHolder> lst = new ArrayList<TranscriptionHolder>();

		@Override
		public void onWrapBegin() {
			map.clear();
			lst.clear();
		}

		@Override
		public void onWrap(Object data) {
			if (data instanceof TranscriptionHolder) {
				TranscriptionHolder holder = (TranscriptionHolder) data;
				if (!map.containsKey(holder.getRevTranscription().toString())) {
					map.put(holder.getRevTranscription().toString(), holder);
				}
//				lst.add(holder);
			}
			
		}

		@Override
		public void onWrapEnd() {
			printOutMap();
			//printoutList();
		}
		
		/**
		 * išspausdina rastus pakeitimus debuginimo tikslais
		 */
		private void printOutMap() {
			String fn = DEBUG_DIR + "/" + Context.getInsTance().getSpeaker() + "_A_byScore.csv";
			List<TranscriptionHolder> holderList = new ArrayList<TranscriptionHolder>(map.values());
			Collections.sort(holderList, new HolderCmpByScore());
			printList(holderList, fn);
			fn = DEBUG_DIR + "/" + Context.getInsTance().getSpeaker() + "_A_byLength.csv";
			Collections.sort(holderList, new HolderCmpBySize());
			printList(holderList, fn);
		}
		
		private void printoutList() {
			String fn = DEBUG_DIR + "/" + Context.getInsTance().getSpeaker() + "_A_byScore.csv";
			List<TranscriptionHolder> holderList = lst;
			Collections.sort(holderList, new HolderCmpByScore());
			printList(holderList, fn);
			fn = DEBUG_DIR + "/" + Context.getInsTance().getSpeaker() + "_A_byLength.csv";
			Collections.sort(holderList, new HolderCmpBySize());
			printList(holderList, fn);
		}
		
		private void printList(List<TranscriptionHolder> holderList, String fn) {
			String sep = ";";
			try {
				PrintStream ps = new PrintStream(new File(fn));
				for (TranscriptionHolder holder : holderList) {
					ps.println(holder.getOrgTranscription().getTranscription().size() + sep + holder.getOrgTranscription().getStart() + sep + holder.getOrgTranscription().getLabName() + sep
							+ holder.getOrgTranscription() + sep + holder.getRevTranscription() + sep
							+ String.format("%.6f", (holder.getRevTranscription().getScore() - holder.getOrgTranscription().getScore()))
							+ sep + holder.getDelta().getType()
							+ sep + holder.getDelta().getOriginal().getLines().size()
							+ sep + holder.getDelta().getRevised().getLines().size()
							+ sep + holder.getDelta().getOriginal().getLines()
							+ sep + holder.getDelta().getRevised().getLines());
				}
				ps.close();
			} catch (FileNotFoundException e) {
				ExceptionHandler.getInstance().handle(e);
			}
		}
		
	}
	
	public class NanScorePrepocessListener implements WrapperListener {

		@Override
		public void onWrapBegin() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onWrap(Object data) {
			if (data instanceof TranscriptionHolder) {
				TranscriptionHolder holder = (TranscriptionHolder) data;
				if (Double.isNaN(holder.getOrgTranscription().getScore())) {
					holder.getOrgTranscription().setScore(-100);
				}
				if (Double.isNaN(holder.getRevTranscription().getScore())) {
					holder.getRevTranscription().setScore(-200);
				}
			}
			
		}

		@Override
		public void onWrapEnd() {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	public class DissapearedSpSkipSlistener implements WrapperListener {

		@Override
		public void onWrapBegin() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onWrap(Object data) {
			if (data instanceof TranscriptionHolder) {
				TranscriptionHolder holder = (TranscriptionHolder) data;
				if (Double.isNaN(holder.getOrgTranscription().getScore())) {
					if (TYPE.DELETE.equals(holder.getDelta().getType()) && holder.getOrgTranscription().getTranscription().size() == 1) {
						Change chg = holder.getOrgTranscription().getTranscription().get(0);
						if ("sp".equals(chg.getMlf().getItem())) {
							log.trace("Found irrelevant tee model delete, skipping...");
							holder.getOrgTranscription().getTranscription().clear();
						}
					}
				}
			}
			
		}

		@Override
		public void onWrapEnd() {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	public class ZerosScoreProcessListener implements WrapperListener {
		
		private final Logger log = LoggerFactory.getLogger(ZerosScoreProcessListener.class);

		@Override
		public void onWrapBegin() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onWrap(Object data) {
			if (data instanceof TranscriptionHolder) {
				TranscriptionHolder holder = (TranscriptionHolder) data;
				if (holder.getScore() == 0) {
					log.trace("excluding 0 score change for {}", holder);
					
					holder.getOrgTranscription().getTranscription().clear();
					holder.getRevTranscription().getTranscription().clear();
				}
			}
			
		}

		@Override
		public void onWrapEnd() {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	public class TranscriptionExpandDeleteOnlyListener implements WrapperListener {

		@Override
		public void onWrapBegin() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onWrap(Object data) {
			if (data instanceof TranscriptionHolder) {
				TranscriptionHolder holder = (TranscriptionHolder) data;
				expand(holder);
				log.debug("Expanded transcription: {}", holder);
			}
			
		}
		
		public void expand(TranscriptionHolder holder) {
			//int size = holder.getOrgTranscription().getTranscription().size();
			addFront(holder);
			addFront(holder);
			addBack(holder);
			addBack(holder);
		}
		
		public void addFront(TranscriptionHolder holder) {
			Mlf rec = getOriginal(holder.getStart().getOrgIdx() - 1);
			if (rec == null) {
				return;
			}
			if (rec.getLeft() == null) {
				return;
			}
			if ("sill".equals(rec.getItem()) || "sil".equals(rec.getItem())) {
				return;
			}
			holder.getOrgTranscription().getTranscription().add(0, new Change(rec, null));
			holder.getStart().decOrg();
		}
		
		public void addBack(TranscriptionHolder holder) {
			Mlf rec = getOriginal(holder.getEnd().getOrgIdx() + 1);
			if (rec == null) {
				return;
			}
			if (rec.getLeft() == null) {
				return;
			}
			if ("sill".equals(rec.getItem()) || "sil".equals(rec.getItem())) {
				return;
			}
			holder.getOrgTranscription().getTranscription().add(new Change(rec, null));
			holder.getEnd().incOrg();
		}

		@Override
		public void onWrapEnd() {
			// TODO Auto-generated method stub
			
		}
		
	}
}
