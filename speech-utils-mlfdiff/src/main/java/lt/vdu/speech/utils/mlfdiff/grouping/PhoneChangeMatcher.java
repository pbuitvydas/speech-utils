package lt.vdu.speech.utils.mlfdiff.grouping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lt.vdu.speech.utils.common.dstructures.Mlf;
import lt.vdu.speech.utils.common.utilities.ClassUtils;
import lt.vdu.speech.utils.mlfdiff.data.TranscriptionHolder;
import difflib.Delta;
import difflib.Delta.TYPE;

//TODO keisti pavadinima i RegexMather
public class PhoneChangeMatcher implements Matcher {
	
	private String pattern;
	private List<TYPE> matchTypeList = new ArrayList<TYPE>();
	
	@Override
	public String toString() {
		return "(patern=" + pattern + ", types=" + matchTypeList + ")";
	}
	
	public List<String> getLines(Delta delta) {
		List<Mlf> recList = new ArrayList<Mlf>();
		if (delta.getType().equals(TYPE.DELETE)) {
			recList = ClassUtils.unwild(delta.getOriginal().getLines().size() > 0 ? delta.getOriginal().getLines()
					: delta.getRevised().getLines());
		} else {
			List<Mlf> listToAdd = ClassUtils.unwild(delta.getOriginal().getLines());
			recList.addAll(listToAdd);
			listToAdd = ClassUtils.unwild(delta.getRevised().getLines());
			recList.addAll(listToAdd);
		}
		List<String> retlList = new ArrayList<String>();
		for (Mlf r : recList) {
			retlList.add(r.getItem());
		}
		return retlList;
	}
	
	protected List<TYPE> getTypesList(Object ...params) {
		List<TYPE> retList = new ArrayList<TYPE>();
		for (Object o : params) {
			if (o instanceof String) {
				retList.add(TYPE.valueOf((String) o ));
			} else if (o instanceof TYPE) {
				retList.add((TYPE) o );
			}
		}
		return retList;
	}

	@Override
	public void setup(String matchRule, Object... params) {
		this.pattern = matchRule;
		if (params.length == 0) {
			matchTypeList.addAll(Arrays.asList(TYPE.values()));
		} else {
			List<TYPE> lst = getTypesList(params);
			matchTypeList.addAll(lst);
		}
		
	}

	@Override
	public boolean matches(TranscriptionHolder holder) {
		Delta delta = holder.getDelta();
		if (matchTypeList.indexOf(delta.getType()) == -1) {
			return false;
		}
		for (String line : getLines(delta)) {
			if (line.matches(pattern)) {
				return true;
			}
		}
		return false;
	}

}
