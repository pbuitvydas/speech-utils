package lt.vdu.speech.utils.mlfdiff.grouping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import lt.vdu.speech.utils.mlfdiff.data.TranscriptionHolder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import difflib.Delta;
import difflib.Delta.TYPE;

/**
 * Grupavimo manageris skirtas infrmacijai surinkti, kur kiekvienas grup4s memberis 
 * kokiam indexe deltaList'e randasi
 * @author pbuitvydas
 *
 */
public class GroupManager {
	
	private static final Logger log = LoggerFactory.getLogger(GroupManager.class);
	
	private List<GroupMember> groupLsit = new ArrayList<GroupMember>();
	private GroupMember defaultGroup = GroupMember.createDefaultGroupMember();
	private Map<String, Member> membersHitMap = new TreeMap<String, Member>();
	
	public GroupManager() {
	}
	
	/**
	 * Passimti grupe nurodan jos memberį, jei tokio nėra gražina bendrinė grupę
	 * į kurią bus talpinami visi nenumatyti memberiai
	 * @param member
	 * @return
	 */
	public GroupMember getGroup(TranscriptionHolder holder) {
		for (GroupMember g : groupLsit) {
			if (g.isMemberOf(holder)) {
				return g;
			}
		}
		return defaultGroup;
	}
	
	public void updateGroupPositions(TranscriptionHolder holder, int position) {
		GroupMember member = getGroup(holder);
		member.addIndex(position);
	}
	
	public boolean addGroup(String group, String matchRule, Object ...params) {
		GroupMember member = getGroupByGroupName(group);
		if (member != null) {
			return member.createMatcher(matchRule, params);
		}
		return groupLsit.add(new GroupMember(group, matchRule, params));
	}
	
	public GroupMember getGroupByGroupName(String groupName) {
		for (GroupMember member : groupLsit) {
			if (member.getGroupName().equals(groupName)) {
				return member;
			}
		}
		return null;
	}
	
	public void resetMembersIndexes() {
		for (GroupMember gm : getGroupList()) {
			gm.getIndexList().clear();
		}
	}

	public List<GroupMember> getGroupList() {
		if (defaultGroup.getIndexList().size() > 0 && !groupLsit.contains(defaultGroup)) {
			groupLsit.add(defaultGroup);
		}
		return groupLsit;
	}

	public Map<String, Member> getMembersHitMap() {
		return membersHitMap;
	}
 	

}
