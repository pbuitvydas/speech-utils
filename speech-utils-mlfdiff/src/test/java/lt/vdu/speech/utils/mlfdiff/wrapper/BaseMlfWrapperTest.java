package lt.vdu.speech.utils.mlfdiff.wrapper;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;

import lt.vdu.speech.utils.common.diff.CommonDiff;
import lt.vdu.speech.utils.common.dstructures.Mlf;
import lt.vdu.speech.utils.common.io.reader.MlfReader;
import lt.vdu.speech.utils.common.utilities.ClassUtils;
import lt.vdu.speech.utils.mlfdiff.data.TranscriptionHolder;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import difflib.Delta;

import static org.junit.Assert.*;

@Ignore
public class BaseMlfWrapperTest {
	
	private static final String CHANGES_DIR = "diff-cases/";

	@Before
	public void setUp() throws Exception {
	}

	public void testWrapBegin()
	 throws Exception {
	
	}
	
	
	@Test
	public void testGetTraanscritpionSingle() throws FileNotFoundException, IOException {
		File tcFile = ClassUtils.getResource(getClass(), CHANGES_DIR + "tc10_a.properties");
		testTranscription(tcFile.getAbsolutePath());
	}
	
	@Test
	public void testGetTraanscritpion() throws FileNotFoundException, IOException {
		Enumeration<URL> enumeration = getClass().getClassLoader().getResources(CHANGES_DIR);
		URL changesUrl = enumeration.nextElement();
		File changesDir = new File(changesUrl.getPath());
		for (File file : changesDir.listFiles()) {
			if (file.getAbsolutePath().contains(".properties")) {
				testTranscription(file.getAbsolutePath());
			}
		}
		
	}
	
	protected void testTranscription(String testCaseDescriptorName) throws FileNotFoundException, IOException {
		Properties prop = new Properties();
		prop.load(new FileInputStream(testCaseDescriptorName));
		System.out.println("Testing " + testCaseDescriptorName + ": " + prop.getProperty("setup.comment"));
		TranscriptionHolder holder = getTranscription(prop);
		compareTranscriptionValues(holder, prop);
	}
	
	protected void compareTranscriptionValues(TranscriptionHolder holder, Properties prop) {
		Long expectedStart = Long.valueOf(prop.getProperty("expected.start"));
		Long expectedStop = Long.valueOf(prop.getProperty("expected.stop"));
		assertEquals("Start does not match", expectedStart, holder.getOrgTranscription().getStart());
		assertEquals("Stop does not match", expectedStop, holder.getOrgTranscription().getStop());
		
		String orgTranscription = prop.getProperty("expected.stable.orgTransCription");
		String revTranscription = prop.getProperty("expected.stable.modTransCription");
		assertEquals("Org transcription does not match", orgTranscription, holder.getOrgTranscription().transcriptionToString());
		assertEquals("Rev transcription does not match", revTranscription, holder.getRevTranscription().transcriptionToString());
		
		//TODO score compare
	}

	protected TranscriptionHolder getTranscription(Properties prop) {
		//ClassUtils.getResource(getClass(), CHANGES_DIR + prop.getProperty("setup.original"))
		InputStream orgStream = ClassUtils.getResourceAsStream(getClass(), CHANGES_DIR + prop.getProperty("setup.original"));
		InputStream revStream = ClassUtils.getResourceAsStream(getClass(), CHANGES_DIR + prop.getProperty("setup.modified"));
		CommonDiff<Mlf> differ = new CommonDiff<Mlf>();
		MlfReader orgReader = new MlfReader(orgStream);
		MlfReader revrReader = new MlfReader(revStream);
		
		differ.diff(orgReader, revrReader);
		orgReader.close();
		revrReader.close();
		
		BaseMlfWrapper wrapper = new BaseMlfWrapper() {
		};
		wrapper.setOriginal(differ.getOriginal());
		wrapper.setRevised(differ.getRevised());
		Delta delta = differ.getPatch().getDeltas().get(0);
		return wrapper.getTraanscription(differ.getOriginal(), differ.getRevised(), delta);
	}

}
