package lt.vdu.speech.utils.mlfdiff.wrapper.textgrid;

import java.util.List;

import org.junit.Ignore;

import junit.framework.TestCase;
import lt.vdu.speech.utils.common.diff.CommonDiff;
import lt.vdu.speech.utils.common.dstructures.Mlf;
import lt.vdu.speech.utils.common.io.reader.MlfReader;
import lt.vdu.speech.utils.mlfdiff.grouping.GroupManager;
import difflib.Delta;

@Ignore
public class TextGridwrapperTest extends TestCase {
	
	private static final String CHANGES_DIR = "D:/speechChangesMake/d/changes/GRID2";
	private static final String MODIFIED_DIR = "D:/speechChangesMake/d/modified/HTK2";
	private static final String ORIGINAL_DIR = "D:/speechChangesMake/d/original/HTK2";

	protected void setUp() throws Exception {
	}

	public void testWrapBody()
	 throws Exception {
	
	}
	
	public void testPrintAllChanges() {
		String speakers = "ARM BRU BUC CEK DEK DOV JOK JUK LAT LIP LUK MIC NAS RUP SAA SAD SAK SAL URB VID ADO ANI BLA BRA BRD BRO BUL BUT CIZ EID GAD KAM KAR KAZ KOS KUB LEO LIU MAK MAL MAR MEI PLA RAD RAV ROS SIR SIT SKA SMA";
		String[] splits = speakers.split(" ");
		for (String speaker : splits) {
			printAllChanges(speaker);
		}
	}
	
	public void printAllChanges(String speaker) {
    	MlfReader originalReader = new MlfReader(getMlfPathFor(speaker, ORIGINAL_DIR));
    	MlfReader revisedReader = new MlfReader(getMlfPathFor(speaker, MODIFIED_DIR));
    	CommonDiff<Mlf> differ = new CommonDiff<Mlf>();
    	differ.diff(originalReader, revisedReader);
    	originalReader.close();
    	revisedReader.close();
    	List<Mlf> originalList = differ.getOriginal();
    	List<Mlf> revisedList = differ.getRevised();
    	GroupManager gm=null;// = new TextGridwrapper().initGroup();
    	for (int i = 0; i < differ.getPatch().getDeltas().size(); i++) {
    		Delta d = differ.getPatch().getDeltas().get(i);
    		Mlf original = originalList.get(d.getOriginal().getPosition());
    		//gm.updateGroupPositions(original.getItem(), i);
    	}
    	
    	List<Integer> chgs = gm.getGroupList().get(0).getIndexList();
    	for (int i = 0; i < chgs.size(); i++) {
    		int index = chgs.get(i);
    		Delta d = differ.getPatch().getDeltas().get(index);
    		Mlf original = originalList.get(d.getOriginal().getPosition());
    		Mlf revised = revisedList.get(d.getRevised().getPosition());
    		System.out.println(original.getItem() + ", " + revised.getItem());
    	}
    	
	}
	
	public String getMlfPathFor(String speaker, String dir) {
		return dir + "/" + speaker + "_A.mlf";
	}

}
