@echo off
:: Startup script for the application

call "%~dp0findjava"
if "%_JAVACMD%"=="" goto noJava

:: run actual application
"%_JAVACMD%" -Dlog4j.configuration=conf\log4j.properties -jar "%~dp0..\lib\${project.name}-${project.version}.jar" %*
goto end

:noJava
@echo Java is needed in order to run this app, please install.

:end