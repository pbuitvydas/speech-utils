package lt.vdu.speech.utils.tgrid2dic;

import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;

public class Main {

	public static void main(String[] args) {
		if (args.length != 5) {
			printHelp();
		} else {
			try {
				new DictionaryGenerator().generate(args[0], args[1], args[2], args[3], args[4]);
			} catch (DicitonaryGenException e) {
				ExceptionHandler.getInstance().handle(e);
			}
		}

	}
	
	public static void printHelp() {
		System.out.println("Generate frequency dictionary from Pratt TextGrid files" 
				+ "\nUsage:" 
				+ "\ntexGridDir textGridExtension dictionaryFile wordsTierName phonesTierName" 
				+ "\nExample:" 
				+ "\nC:\\data TextGrid dictionary.dic zodziai garsai");
	}

}
