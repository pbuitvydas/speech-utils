package lt.vdu.speech.utils.tgrid2dic;

import java.io.File;
import java.io.IOException;

import lt.vdu.speech.utils.common.dstructures.DictEntry;
import lt.vdu.speech.utils.common.dstructures.Dictionary;
import lt.vdu.speech.utils.common.dstructures.textgrid.Interval;
import lt.vdu.speech.utils.common.dstructures.textgrid.Item;
import lt.vdu.speech.utils.common.dstructures.textgrid.TextGrid;
import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;
import lt.vdu.speech.utils.common.io.reader.TextGridReader;
import lt.vdu.speech.utils.common.io.writer.DictionaryWriter;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DictionaryGenerator {
	
	private static final Logger log = LoggerFactory.getLogger(DictionaryGenerator.class);
	
	private Dictionary dictionary;
	private String wordsTierName;
	private String phonesTierName;
	
	public void generate(String textGridDir, String tGridExtension, String dictionaryFile
			, String wordsTierName, String phonesTierName) throws DicitonaryGenException {
		this.wordsTierName = wordsTierName;
		this.phonesTierName = phonesTierName;
		dictionary = getDictionary(dictionaryFile);
		File data = new File(textGridDir);
		if (!data.exists()) {
			throw new DicitonaryGenException("path " + textGridDir + " does not exist");
		}
		log.info("Generating...");
		if (data.isDirectory()) {
			log.info("Walking file tree in {}", textGridDir);
			for (File f : FileUtils.listFiles(data, new String[] {tGridExtension}, true)) {
				processGridFile(f);
			}
		} else {
			
			processGridFile(data);
		}
		log.info("Writing generated dictionary to {}", dictionaryFile);
		writeDictionary(dictionaryFile);
		log.info("Done.");
		
	}
	
	protected void writeDictionary(String dictionaryF) {
		DictionaryWriter writer = new DictionaryWriter(dictionaryF);
		dictionary.generateProbabilities();
		writer.writeDicitonary(dictionary, true);
		writer.close();
	}
	
	protected void processGridFile(File gridF) throws DicitonaryGenException {
		log.debug("Processing grid {}", gridF);
		TextGridReader gridReader = new TextGridReader(gridF.getAbsolutePath());
		try {
			TextGrid grid = gridReader.readData();
			gridReader.close();
			exctractWords(grid);
		} catch (IOException e) {
			ExceptionHandler.getInstance().handle(e);
		}
	}
	
	protected void exctractWords(TextGrid grid) throws DicitonaryGenException {
		Item wordsTier = grid.getItem(wordsTierName);
		Item phonesTier = grid.getItem(phonesTierName);
		if (wordsTier == null) {
			throw new DicitonaryGenException("Could not find tier " + wordsTierName);
		}
		if (phonesTier == null) {
			throw new DicitonaryGenException("Could not find tier " + phonesTierName);
		}
		for (Interval word : wordsTier.intervals) {
			putWord(word, phonesTier);
		}
	}
	
	protected void putWord(Interval word, Item phonesTier) {
		String pron = getPronounciation(word, phonesTier);
		dictionary.put(new DictEntry(word.text, pron));
		
	}
	
	protected String getPronounciation(Interval word, Item phonesTier) {
		log.trace("Getting pronounciation for {}", word);
		String retString = "";
		for (Interval phoneme : phonesTier.intervals) {
			if (phoneme.range.xmin >= word.range.xmin && phoneme.range.xmax <= word.range.xmax) {
				retString += normaliseStr(phoneme.text) + " ";
			}
			
		}
		log.trace("Found pronounciation {}", retString);
		return retString.trim();
	}
	
	protected String normaliseStr(String str) {
		return str.replace("\"\"", "\\\"");
	}
	
	protected Dictionary getDictionary(String dictionaryFile) {
		if (new File(dictionaryFile).exists()) {
/* TODO kolkas neaišku kaip papildyt egzistuojantį žodyną todėl paduotas egzistujantis žodynas bus ištrintas*/
//			log.info("Reading dictionary from {}", dictionaryFile);
//			DictionaryReader reader = new DictionaryReader(dictionaryFile);
//			Dictionary retDict = reader.readDictionary();
//			reader.close();
//			log.info("Done reading {} dictionary entries", dictionary.getEntryCount());
//			return retDict;
			log.warn("Suplied dictionary file exists it will be overwritten");
			return new Dictionary();
		}
		return new Dictionary();
	}

}
