package lt.vdu.speech.utils.tgrid2dic;

import lt.vdu.speech.utils.common.exception.SpeechUtilsException;

public class DicitonaryGenException extends SpeechUtilsException {

	private static final long serialVersionUID = -6531928538753119886L;

	public DicitonaryGenException(String description) {
		super(description);
	}

}
