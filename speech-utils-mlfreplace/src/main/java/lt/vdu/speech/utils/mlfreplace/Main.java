package lt.vdu.speech.utils.mlfreplace;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lt.vdu.speech.utils.common.encoding.Encoding;
import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;
import lt.vdu.speech.utils.common.io.reader.StringReader;
/**
 * MLF traanskripcijose esanciu zodziu sandurų pakeitimas į satmbesnį vienetą
 * pvz
 * s
 * sp
 * Z
 * pakeiciama i
 * bn_s_sp_Z
 * paduodamas mlf ir zodynas nurodoma pakeitimu prefiksai kurios naudoti pvz bn
 * @author pbuitvydas
 *
 */
public class Main {
	
	private static final Logger log = LoggerFactory.getLogger(Main.class);
	
	public static HashMap<String, Integer> chFreqMap = new HashMap<String, Integer>();
	public static int hitCount = 0;
	
	public static final String CH_FREQ_FN = "logs/chFreq.txt";
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//mlffile dictionary entryPrefix outfile
		//start(args);
		if (args.length != 4) {
			System.out.println("usage:\nmlffile dictionaryFile dictionaryentryPrefix outfile");
		}
		else {
			start(args);

		}

	}
	
	public static void restoreMap(String fileName) {
		File f = new File(fileName);
		if (f.exists()) {
			List<String> lst = new StringReader(fileName).readAsList();
			for (String str : lst) {
				String[] splits = str.split("\t");
				if (splits.length == 2) {
					String key = splits[0];
					Integer value = Integer.parseInt(splits[1]);
					chFreqMap.put(key, value);
				}
			}
		}
	}
	
	public static void updateChFreqMap(String key) {
		hitCount++;
		if (chFreqMap.containsKey(key)) {
			chFreqMap.put(key, chFreqMap.get(key)+1);
		} else {
			chFreqMap.put(key, 1);
		}
	}
	
	public static void saveChFreqMap() {
		try {
			BufferedWriter wr = new BufferedWriter(new FileWriter(new File(CH_FREQ_FN)));
			for (String key : chFreqMap.keySet()) {
				wr.write(key + "\t" + chFreqMap.get(key) + "\n");
				
			}
			wr.close();
		} catch (IOException e) {
			ExceptionHandler.getInstance().handle(e);
		}
	}
	
	public static void start(String[] args) {
		try {
			
			restoreMap(CH_FREQ_FN);
			List<Entry> entries = getEntryList(args[1], args[2]);
			start(args[0], args[3], entries);	
			
			saveChFreqMap();
			log.info("Total changes made: {}", hitCount);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void start(String inFn, String outFn, List<Entry> entries) throws IOException {
		log.info("processing: {}", inFn);
		InputStreamReader streamReader = new InputStreamReader(new FileInputStream(new File(inFn)), Encoding.defaultCharset());
		BufferedReader reader = new BufferedReader(streamReader );
		File tmpFile = new File(outFn+"_tmp");
		File targFile = new File(outFn);
		OutputStreamWriter outWriter = new OutputStreamWriter(new FileOutputStream(tmpFile), Encoding.defaultCharset());
		BufferedWriter writer = new BufferedWriter(outWriter);
		String line = null, previousLine = null, nextLine = null;
		List<String[]> cache = new ArrayList<String[]>();
		boolean replaced = false;
		List<String> contents = new ArrayList<String>();
		do {
			line = reader.readLine();
			if (line == null) continue;
			contents.add(line);
		}while(line != null);
		for (int i = 0; i < contents.size(); i++) {
			
			line = contents.get(i);
			String [] splits = line.split(" +", 3);
//			if (splits.length == 1) {
//				writer.write(line+"\n");
//				continue;
//			}
			int ofset = splits.length == 3 ? 1 : 0;
			if (splits[0+ofset].equals(entries.iterator().next().eStruct.middle)) {//ar lygi sp
				String previous = i-1 != 0 ? contents.get(i-1) : null;
				boolean has = i-2 >= 0 ? contents.get(i-2).contains("sp") : false;
				previous = has ? null : previous;
				String next = i++ < contents.size() ? contents.get(i) : null;
				if (previous!= null && next!=null) {
					cache.add(previous.split(" +", 3));
					cache.add(splits);
					cache.add(next.split(" +", 3));
					cache = replace(cache, entries);
					

				}
				if (cache.size() == 1) {
					updateChFreqMap(cache.get(0)[0]);
					log.debug("making change {} at line {}", cache.get(0)[0], i);
					writer.write(cache.get(0)[0]+"\n");
				}
				else {
					if (previous != null) {
						writer.write(previous+"\n");
					}
					writer.write(line+"\n");
					if (next != null) {
						writer.write(next+"\n");
					}
				}
				cache = new ArrayList<String[]>();
			}
			else {
				String tmp = i+1 < contents.size() ? contents.get(i+1) : null;
				if (tmp != null) {
					splits = tmp.split(" +", 3);
					ofset = splits.length == 3 ? 1 : 0;
					EntryStruct estruct = entries.iterator().next().eStruct;
					if (isPhoneOneWord(contents, i)) {
						writer.write(line+"\n");
					} else if (!splits[0+ofset].equals(estruct.middle)) {//ar lygi sp
						writer.write(line+"\n");//cia greiciausiai bugas pamato o: sp ir praleidzia
					}
					
				}
				else {
					writer.write(line+"\n");
				}
			}
		}
		writer.close();
		reader.close();
		if (targFile.exists()) {
			targFile.delete();
		}
		if (tmpFile.exists()) {
			tmpFile.renameTo(targFile);
		}
		//perkelti faila;
	}
	
	public static boolean isPhoneOneWord(List<String> contents, int i) {
		String tmpa = i+1 < contents.size() ? contents.get(i+1) : null;
		String tmpb = i-1 > 0 ? contents.get(i-1) : null;
		if (splitsContains(tmpa, "sp") && splitsContains(tmpb, "sp")) {
			return true;
		}
		return false;
	}
	
	public static boolean splitsContains(String toSplit, String compare) {
		String[] splits = toSplit.split(" +");
		for (String str : splits) {
			if (str.equals(compare)) {
				return true;
			}
		}
		return false;
	}
	
	public static List<String[]> replace(List<String[]> cache, List<Entry> entries) {
		Entry e = getEntryForReplace(cache, entries);
		if (e==null) {
			return cache;
		}
		String[] arr = new String[] { e.meaning };
		return Collections.singletonList(arr);//TODO ka daryt su papildoma informacija
	}
	
	public static Entry getEntryForReplace(List<String[]> cache, List<Entry> entries) {
		int offset = cache.get(0).length == 3 ? 1 : 0;
		String previous = cache.get(0)[0+offset];
		String middle = cache.get(1)[0+offset];
		String next = cache.get(2)[0+offset];
		for (Entry e : entries) {
			if (e.eStruct.previous.equals(previous) &&
					e.eStruct.middle.equals(middle) && 
					e.eStruct.next.equals(next)) {
				return e;
			}

		}
		return null;
	}
	
	public static List<Entry> getEntryList(String dicionaryFn, String entryPrefixes) throws IOException {
		List<String> prefixes = Arrays.asList(entryPrefixes.split(","));
		List<Entry> entryList = new ArrayList<Entry>();
		InputStreamReader streamReader = new InputStreamReader(new FileInputStream(new File(dicionaryFn)), Encoding.defaultCharset());
		BufferedReader reader = new BufferedReader(streamReader );
		String line = null;
		do {
			line = reader.readLine();
			if (line == null) {
				continue;
			}
			String[] entryStrMas = line.split(" +", 2);
			if (entryStrMas.length > 1) {
				for (String prefix : prefixes) {
					if (entryStrMas[0].contains(prefix) && !entryStrMas[0].contains("\\\"")) {
						entryList.add(new Entry(entryStrMas[0], entryStrMas[1]));
					}
				}
			}
		}while(line != null);
		reader.close();
		return entryList;
	}
	


}

class Entry {
	public String meaning;
	public String value;
	public EntryStruct eStruct;
	
	public Entry(String meaning, String value) {
		this.eStruct = getEntryStruct(meaning);
		if (meaning.contains("\\")) {
			meaning = meaning.replaceAll("\\\\", "");
			meaning = "'" + meaning + "'";
		}
		this.meaning = meaning;
		this.value = value;
	}
	
	public EntryStruct getEntryStruct(String value) {
		String [] splits = value.split("_", 4);
		EntryStruct struct = new EntryStruct();
		if (splits[1].contains("\\")) {
			splits[1] = splits[1].replaceAll("\\", "");
			splits[1] = "'" + splits[1] + "'";
		}
		struct.previous = splits[1];
		struct.middle = splits[2];
		if (splits[3].contains("\\")) {
			splits[3] = splits[3].replaceAll("\\\\", "");
			splits[3] = "'" + splits[3] + "'";
		}
		struct.next = splits[3];
		return struct;
	}
}

class EntryStruct {
	public String previous;
	public String middle;
	public String next;
}
