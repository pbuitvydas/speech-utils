## Kalbos tyrimuose naudojamų įrankių rinkinys:

CI status:
[![Build Status](https://vendigo.ci.cloudbees.com/buildStatus/icon?job=speech-utils)](https://vendigo.ci.cloudbees.com/job/speech-utils/)

### Katalogų struktūra:
* bin - įrankių paleidžiamieji skriptai
* conf - įrankių konfigūraciniai failai
* lib - java programos, bei bibliotekos

### Įrankai:
* mlfdiff - HTK Mlf skirtumų paieškos bei sisteminimas
* gentrilst - nematytų trifonų mlf failuose generavimas
* mlfreplace - atlikti pakeitimus HTK Mlf failuose
* tgrid2dic - žodyno generavimas iš Praat TextGrid failų
	
### Paleidimas:
* bin\<irankis>.bat <parametrai>
* Nenurodant parametrų bus parodoma mini naudojimosi instrukcija
* DĖMESIO: kviečiant įrankius iš kito bat skripto naudoti: call bin\<irankis>.bat <parametrai>
	
### Reikalavimai:
* Java virtuali mašina JRE minimum 1.6 versija
* Operacinei sistemai įpatingų reikalavimų nėra, 
	  tačiau paleidžiamieji skriptai yra pritakikyti tik WINDOWS šeimos OS,
	  paleidimas kitose operacinėse sistemos yra trivialus, jei tenkinamas pirmas
	  reikalavimas.

### Bendra informacija:
* naudojantis įrankiais bus automatiškai loginamas įrankių darbas
	  <currentDir>\logs\speech-utils.log, šią funkciją galima išjungti žr. įrankių
	  konfigūravimas.
	
### Įrankų konfigūravimas:
* Log lygio keitimas koreguonat failą logs\log4j.properties žr. failo viduje esančias rekomendacijas
* mlfdiff įrankio konfigūravimas naudojanst mlfdiff.xml