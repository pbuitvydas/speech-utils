package lt.vdu.speech.utils.common.io.writer;

import java.io.File;

public class StringWriter extends BasicBufferedWriter<String> {

	public StringWriter(String fileName) {
		super(fileName);
	}
	
	public StringWriter(File file) {
		super(file);
	}

}
