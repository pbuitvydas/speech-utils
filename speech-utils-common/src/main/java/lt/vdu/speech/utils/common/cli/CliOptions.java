package lt.vdu.speech.utils.common.cli;

import java.io.OutputStream;
import java.io.PrintWriter;

import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;

/**
 * Konsolės parametrų pagalbinė klasė
 * @author pbuitvydas
 *
 */
public class CliOptions {
	
	private Options options;
	private Class<?> mainClass;
	
	/**
	 * Standartinis konstruktorius
	 * @param options - parametrai
	 * @param mainClass - pagrindinė klasė naudojama helpo printinimui
	 */
	public CliOptions(Options options, Class<?> mainClass) {
		this.options = options;
		this.mainClass = mainClass;
	}
	
	/**
	 * Naudojimo zinutes printinimas
	 * @param applicationName - aplikacijos pavadinimas
	 * @param options - parametrai
	 * @param out - outputSetream'as pvz stdout
	 */
	public void printUsage(String applicationName, Options options, 
			OutputStream out) {
		PrintWriter writer = new PrintWriter(out);
		HelpFormatter usageFormatter = new HelpFormatter();
		usageFormatter.printUsage(writer, 80, applicationName, options);
		writer.close();
	}
	
	/**
	 * Išsamaus helpo standartinis printinimas
	 */
	public void printHelpStd() {
		printHelp(80, "Parameters:", "", 3, 5, true, System.out);
	}
	
	/**
	 * Parametrizuotas helpo printinimas 
	 * @param printedRowWidth
	 * @param header
	 * @param footer
	 * @param spacesBeforeOption
	 * @param spacesBeforeOptionDescription
	 * @param displayUsage
	 * @param out
	 */
	public void printHelp(int printedRowWidth, String header,
			String footer, int spacesBeforeOption,
			int spacesBeforeOptionDescription,
			boolean displayUsage, final OutputStream out) {
		String commandLineSyntax = getSampleUsage();
		final PrintWriter writer = new PrintWriter(out);
		final HelpFormatter helpFormatter = new HelpFormatter();
		helpFormatter.printHelp(writer, printedRowWidth, commandLineSyntax,
				header, getOptions(), spacesBeforeOption,
				spacesBeforeOptionDescription, footer, displayUsage);
		writer.flush();
		//writer.close();
	}
	
	/**
	 * Parametrų geteris
	 * @return
	 */
	public Options getOptions() {
		return this.options;
	}
	
	/**
	 * Grazina jaro pavadinima
	 * @return
	 */
	public String getSampleUsage() {
		return CliUtils.getExecutingResourceName(mainClass);
	}
}
