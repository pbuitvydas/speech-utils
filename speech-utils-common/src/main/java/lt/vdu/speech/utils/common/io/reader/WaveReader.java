package lt.vdu.speech.utils.common.io.reader;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import lt.vdu.speech.utils.common.dstructures.Buffer;
import lt.vdu.speech.utils.common.dstructures.wave.WaveHeader;

public class WaveReader {
	
	private WaveHeader waveHeader;
	private InputStream waveStream;
	private int bytesPersecond;
	private File waveFile;
	
	public WaveReader() {
		
	}
	
	public WaveReader(String fileName) throws IOException, UnsupportedAudioFileException {
		open(fileName);
	}
	
	public void open(String fileName) throws IOException, UnsupportedAudioFileException {
		waveHeader = new WaveHeader();
		waveHeader.readHeader(fileName);
		File waveFile = new File(fileName);
		AudioFileFormat fileFormat = AudioSystem.getAudioFileFormat(waveFile);
		waveHeader.setFrameSize(fileFormat.getFormat().getFrameSize());
		waveHeader.setFrameRate(fileFormat.getFormat().getFrameRate());
		this.waveFile = waveFile;
		waveStream = AudioSystem.getAudioInputStream(waveFile);
		bytesPersecond = waveHeader.getFrameSize() * (int) waveHeader.getFrameRate();
		
	}
	
	public void close() throws IOException {
		if (waveStream != null) {
			waveStream.close();
		}
		
	}
	
	public Buffer read(float startSecond, float stopSecond) throws IOException, UnsupportedAudioFileException {
		int start = (int)(startSecond * bytesPersecond);
		int len = (int)((stopSecond - startSecond) * bytesPersecond);
		return read(start, len);
	}
	
	public Buffer read(long start, int len) throws IOException, UnsupportedAudioFileException {
		/*TODO skip metodas skipina nuo esamos tai netinka nes visada reikia seekint nuo 0
		 *todel vis atidarinejamas naujas streamas; Reikia perdaryti su RandomAccessFile
		 * 
		 */
		waveStream = AudioSystem.getAudioInputStream(waveFile);
		waveStream.skip(start);
		Buffer b = new Buffer(len+1); //nuskaito vienu maziau
		b.dataSize = waveStream.read(b.data); //gali nuskaityti mazaiu nei nurodytas
		return b;
	}

	
	public WaveHeader getWaveHeader() {
		return waveHeader;
	}

}
