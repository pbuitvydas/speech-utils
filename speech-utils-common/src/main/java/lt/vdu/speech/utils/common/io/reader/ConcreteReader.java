package lt.vdu.speech.utils.common.io.reader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;


public class ConcreteReader extends BaseReader<BufferedReader, Integer> {

	public ConcreteReader(String fileName) {
		super(fileName);
	}
	
	@Override
	protected BufferedReader createReader() throws FileNotFoundException {
		return new BufferedReader(new FileReader(getFile()));
	}

	@Override
	public Integer readItem() throws Exception {
		return null;
	}

}
