package lt.vdu.speech.utils.common.encoding;

import java.nio.charset.Charset;

/**
 * Charset'o helper klase, naudojama nustatyti defaultinį charset'ą,
 * kadangi dauguma failų su lietuviškomis reidėmis naudoja CP1257 charsetą
 * standartinis java UTF-8 netinka
 * 
 * @author pbuitvydas
 *
 */
public class Encoding {
	
	private static String charset  = "cp1257";
	
	public static Charset defaultCharset() {
		return Charset.forName(charset);
	}
	
	public static void setCharsetName(String charsetName) {
		if (charsetName != null) {
			charset = charsetName;
		}
	}
	
	public static String getCharsetName() {
		return charset;
	}
}
