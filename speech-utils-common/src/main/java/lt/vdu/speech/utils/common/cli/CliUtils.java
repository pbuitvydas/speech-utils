package lt.vdu.speech.utils.common.cli;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;

public class CliUtils {
	
	/**
	 * Gauti resurso pavadinima, jei jar'as, jei ne pateiktos klasės pavadinimą 
	 * @param clazz
	 * @return
	 */
	public static String getExecutingResourceName(Class<?> clazz) {
		String path = clazz.getProtectionDomain().getCodeSource().getLocation().getPath();
		String decodedPath = "";
		try {
			decodedPath = URLDecoder.decode(path, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			ExceptionHandler.getInstance().handle(e);
		}
		File file = new File(decodedPath);
		if (file.isDirectory()) {
			return clazz.getSimpleName();
		}
		return file.getName();
	}

}
