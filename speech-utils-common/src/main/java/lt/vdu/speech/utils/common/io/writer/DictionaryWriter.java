package lt.vdu.speech.utils.common.io.writer;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import lt.vdu.speech.utils.common.dstructures.DictEntry;
import lt.vdu.speech.utils.common.dstructures.Dictionary;
import lt.vdu.speech.utils.common.exception.WordNotFoundException;
import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;
import lt.vdu.speech.utils.common.sorting.LocalisedStringComparator;

public class DictionaryWriter extends BasicBufferedWriter<DictEntry> {

	public DictionaryWriter(File file) {
		super(file);
	}

	public DictionaryWriter(String fileName) {
		super(fileName);
	}

	public void writeDicitonary(Dictionary dictionary, boolean sortEntries) {
		Set<String> words = dictionary.getWords();
		List<String> wordList = new ArrayList<String>(words);
		if (sortEntries) {
			Collections.sort(wordList, new LocalisedStringComparator());
		}
		try {
			for (String word : wordList) {
				for (DictEntry entry : dictionary.getEntries(word)) {
					write(entry);
				}
			}
		} catch (WordNotFoundException e) {
			ExceptionHandler.getInstance().handle(e);
		}
	}

}
