package lt.vdu.speech.utils.common.dstructures;

/**
 * Mlf item'o klasė
 * @author pbuitvydas
 *
 */
//TODO pagal htk book p. 93 : 6.2.1 HTK Label Files
//mlf structūra turi būt
//[start [end] ] name [score] { auxname [auxscore] } [comment]
public class Mlf {
	
	private String line;//nuskaityta eilute optimizacijai
	private String left;//kaire puse
	private String item;//itemas, pvz.: fonema ar labo pavadinimas
	private String right;//desine puse
	private String labName;//lab pavadinimas performanso sumetimais
	
	private long start;
	private long stop;
	private float proba;
	
	public Mlf() {
		
	}
	
	@Deprecated
	public Mlf(String left, String item, String right) {
		this.left = left;
		this.item = item;
		this.right = right;
	}
	
	@Deprecated
	public Mlf(String item) {
		this.item = item;
	}
	
	public Mlf(String line, String name) {
		this.line = line;
		this.item = name;
	}
	
	@Override
	public String toString() {
		return line;
	}
	
	/**
	 * laikomi lygiais jei vidurine dalis sutampa
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj instanceof Mlf) {
			Mlf other = (Mlf)obj;
			return this.item.equals(other.item);
		}
 		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		return item.hashCode();
	}
	
	/**
	 * palyginimas pagal visus laukus
	 * @param other
	 * @return
	 */
	public boolean equalsByFields(Mlf other) {
		if (other == null) {
			return false;
		}
		return start == other.getStart2() && stop == other.getStop2() && proba == other.getProba()
				&& getItem().equals(other.getItem());
	}

	public String getLeft() {
		return left;
	}

	public void setLeft(String left) {
		this.left = left;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getRight() {
		return right;
	}

	public void setRight(String right) {
		this.right = right;
	}

	public String getLabName() {
		return labName;
	}

	public void setLabName(String labName) {
		this.labName = labName;
	}

	public long getStart2() {
		return start;
	}

	public void setStart2(long start) {
		this.start = start;
	}

	public long getStop2() {
		return stop;
	}

	public void setStop2(long stop) {
		this.stop = stop;
	}

	public float getProba() {
		return proba;
	}

	public void setProba(float proba) {
		this.proba = proba;
	}

}
