package lt.vdu.speech.utils.common.utilities;

import java.io.File;
import java.io.FileFilter;

public class FileFilterByPattern implements FileFilter {
	
	private String pattern;
	
	public FileFilterByPattern(String pattern) {
		this.pattern = pattern;
	}

	@Override
	public boolean accept(File pathname) {
		return pathname.getName().matches(pattern);
	}

}
