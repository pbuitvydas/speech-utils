package lt.vdu.speech.utils.common.io.writer;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.List;

public class BasicWriterStdOut<T> extends BaseWriter<PrintStream, T> {

	@Override
	protected PrintStream createWriter() throws Exception {
		return System.out;
	}

	@Override
	protected void closeWriter() throws Exception {		
	}
	
	@Override
	public void writeString(String str) throws Exception {
		writer.println(str);
	}
	
	public void write(T item) throws Exception {
		writer.println(item.toString());
	}
	
	@Override
	public void write(List<T> items) throws Exception {
		for (T item : items) {
			write(item);
		}
	}

}
