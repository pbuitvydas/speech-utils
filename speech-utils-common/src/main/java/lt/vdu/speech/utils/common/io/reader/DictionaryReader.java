package lt.vdu.speech.utils.common.io.reader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.MultiMap;

import lt.vdu.speech.utils.common.dstructures.DictEntry;
import lt.vdu.speech.utils.common.dstructures.Dictionary;
import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;

/**
 * Htk žodyno readeris
 * @author pbuitvydas
 *
 */
//TODO implmentuoti pagal referenc'ą is htk knygos
public class DictionaryReader extends BasicBufferedReader<DictEntry> {

	public DictionaryReader(String fileName) {
		super(fileName);
	}
	
	/**
	 * @deprecated keisti į readItem2()
	 */
	@Deprecated
	@Override
	public DictEntry readItem() throws Exception {
		DictEntry entry = new DictEntry();
		String str = readAsString();
		if (str == null) {
			return null;
		}
		String[] splits = str.split(" ", 2);
		splits[0] = splits[0].replace("\\", "");
		entry.setWord(splits[0]);
		entry.setTranscript(splits[1]);
		return entry;
	}
	
	public DictEntry readItem2() throws Exception {
		DictEntry entry = new DictEntry();
		String str = readAsString();
		if (str == null) {
			return null;
		}
		String[] splits = str.split(" ", 2);
		entry.setWord(splits[0].trim());
		entry.setTranscript(splits[1].trim());
		return entry;
	}
	
	@Override
	public List<DictEntry> readAsList() {
		List<DictEntry> list = new ArrayList<DictEntry>();
		DictEntry entry;
		try {
			while ((entry = readItem()) != null) {
				list.add(entry);
			}
		} catch (Exception e) {
			ExceptionHandler.getInstance().handle(e);
		}
		Collections.sort(list);
		return list;
	}
	
	public Dictionary readDictionary() {
		Dictionary dictionary = new Dictionary();
		DictEntry entry;
		try {
			while ((entry = readItem2()) != null) {
				dictionary.put(entry);
			}
		} catch (Exception e) {
			ExceptionHandler.getInstance().handle(e);
		}
		return dictionary;
	}

}
