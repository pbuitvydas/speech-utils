package lt.vdu.speech.utils.common.exception.handler;

import java.io.FileNotFoundException;
import java.io.IOException;

import lt.vdu.speech.utils.common.exception.SpeechUtilsException;
import lt.vdu.speech.utils.common.exception.WordNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExceptionHandler {
	
	private static final Logger log = LoggerFactory.getLogger(ExceptionHandler.class);
	
	public void handle(Exception ex) {
		
		if (ex instanceof FileNotFoundException) {
			handleFileNotFoundException((FileNotFoundException) ex);
		} else if (ex instanceof WordNotFoundException) {
			handleWordNotFoundException((WordNotFoundException) ex); 
		} else if (ex instanceof SpeechUtilsException) {
			handleSpeechUtilsException((SpeechUtilsException) ex);
		} else {
			die(ex);
		}
	}
	
	protected void handleWordNotFoundException(WordNotFoundException ex) {
		die(ex);
	}
	
	protected void handleSpeechUtilsException(SpeechUtilsException ex) {
		die(ex);
	}
	
	protected void handleFileNotFoundException(FileNotFoundException ex) {
		die(ex);
	}
	
	protected void die(Exception ex) {
		System.err.println("Runtime exception occured");
		log.error("Exception occured: ", ex);
		System.err.println("Terminating program");
		System.exit(-1);
	}
	
	public static ExceptionHandler getInstance() {
		return new ExceptionHandler();
	}

}
