package lt.vdu.speech.utils.common.io.writer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import lt.vdu.speech.utils.common.dstructures.Buffer;
import lt.vdu.speech.utils.common.dstructures.wave.WaveHeader;
import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;

public class WaveWriter {
	
	private WaveHeader waveHeader;
	private RandomAccessFile outputStream;
	private int dataSize;
	
	private WaveWriter(String fileName) throws FileNotFoundException {
		File file = new File(fileName);
		if (file.exists()) {
			file.delete();
		}
		outputStream = new RandomAccessFile(new File(fileName), "rw");
	}
	
	public WaveWriter(String fileName, WaveHeader header) throws IOException {
		this(fileName);
		this.waveHeader = header;
		if (waveHeader != null) {
			outputStream.write(waveHeader.getHeaderData());
		}
	}
	
	public void close() throws IOException {
		if (outputStream != null) {
			outputStream.close();
			outputStream = null;
		}
	}
	
	/**
	 * Iraso baitu masyva i wav faila paupdeitina headeri
	 * @param data
	 * @return
	 * @throws IOException
	 */
	public int write(byte [] data) throws IOException {
		dataSize = 0;
		outputStream.write(data);
		updateHeaderSize(data.length);
		return 0;
	}
	
	/**
	 * Papildo wav faila paupdeitina headeri
	 * @param data baitu masyvas
	 * @return
	 * @throws IOException
	 */
	public int append(Buffer buffer) throws IOException {
		outputStream.write(buffer.data, 0, buffer.dataSize);
		updateHeaderSize(buffer.dataSize);
		return 0;
	}
	
	public void updateHeaderSize(int size) throws IOException {
		long currentPos = outputStream.getFilePointer();
		dataSize += size;
		outputStream.seek(4);
		outputStream.write(toLittleEndian(36 + dataSize));
		outputStream.seek(40);
		outputStream.write(toLittleEndian(dataSize));
		outputStream.seek(currentPos);
	}
	
	public WaveWriter(WaveHeader header) {
		this.waveHeader = header;
	}
	
	public byte[] toLittleEndian(int bigEndian) {
		byte [] b = new byte[4];
		ByteBuffer bt = ByteBuffer.wrap(b);
		bt.order(ByteOrder.LITTLE_ENDIAN);
		bt.putInt(bigEndian);
		return b;
	}

	public WaveHeader getWaveHeader() {
		return waveHeader;
	}

	public void setWaveHeader(WaveHeader waveHeader) {
		if (this.waveHeader == null) {
			try {
				outputStream.write(waveHeader.getHeaderData());
			} catch (IOException e) {
				ExceptionHandler.getInstance().handle(e);
			}
		}
		this.waveHeader = waveHeader;
	}

}
