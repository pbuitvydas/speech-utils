package lt.vdu.speech.utils.common.dstructures;

public class Buffer {
	
	public byte[] data;
	public int dataSize;
	
	public Buffer(int size) {
		data = new byte[size];
		dataSize = 0;
	}

}
