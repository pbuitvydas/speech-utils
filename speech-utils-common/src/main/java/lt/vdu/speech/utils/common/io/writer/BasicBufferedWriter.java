package lt.vdu.speech.utils.common.io.writer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

import lt.vdu.speech.utils.common.encoding.Encoding;
import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;

public class BasicBufferedWriter<T> extends BaseWriter<BufferedWriter, T> {
	
	private String newLine = "\r\n";

	public BasicBufferedWriter(String fileName) {
		super(fileName);
	}
	
	public BasicBufferedWriter(File file) {
		super(file);
	}

	@Override
	protected BufferedWriter createWriter() throws Exception {
		OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(getFile()), Encoding.defaultCharset());
		return new BufferedWriter(osw);
	}

	@Override
	protected void closeWriter() throws Exception {
		if (getWriter() != null) {
			getWriter().close();
		}
	}
	
	public void write(T item) {
		try {
			writeString(item.toString() + newLine);
		} catch (Exception e) {
			ExceptionHandler.getInstance().handle(e);
		}
	}
	
	@Override
	public void write(List<T> items) {
		if (items == null) {
			return;
		}
		for (int i = 0; i < items.size(); i++) {
			write(items.get(i));
		}
	}
	
	@Override
	public void writeString(String str) throws Exception {
		try {
			writer.write(str);
		} catch (Exception e) {
			ExceptionHandler.getInstance().handle(e);
		}
	}
	
	public String newLine() {
		return newLine;
	}

}
