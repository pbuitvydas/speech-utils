package lt.vdu.speech.utils.common.io.reader;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import lt.vdu.speech.utils.common.encoding.Encoding;

/**
 * Bazinis buferizuotas failų readeris
 * 
 * @author pbuitvydas
 *
 * @param <T> - Struktura į kuria bus skaitomi duomenys
 */
//TODO konstruktorius is failo
public class BasicBufferedReader<T> extends BaseReader<BufferedReader, T> {
	
	private long lineNo = 0;

	public BasicBufferedReader(String fileName) {
		super(fileName);
	}
	
	public BasicBufferedReader(InputStream inputStream) {
		super(inputStream);
	}
	
	@Override
	protected BufferedReader createReader() throws Exception {
		InputStreamReader streamReader = null;
		if (getFile() != null) {
			streamReader = new InputStreamReader(new FileInputStream(getFile()), getCharset());
		} else {
			streamReader = new InputStreamReader(getStream(), getCharset());
		}
		return new BufferedReader(streamReader);
	}
	
	@Override
	protected void closeReader() throws Exception {
		if (reader != null) {
			reader.close();
		}
	}
	
	@Override
	public String readAsString() throws Exception {
		lineNo++;
		return reader.readLine();
	}

	public long getLineNo() {
		return lineNo;
	}
	
	public Charset getCharset() {
		return Encoding.defaultCharset();
	}

}
