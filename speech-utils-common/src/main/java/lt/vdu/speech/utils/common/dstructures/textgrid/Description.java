package lt.vdu.speech.utils.common.dstructures.textgrid;

import lt.vdu.speech.utils.common.format.Appender;
import lt.vdu.speech.utils.common.format.Formattable;

/**
 * TextGrid'o aprašas
 * @author pbuitvydas
 *
 */
public class Description implements Formattable {
	public static final String DEFAULT_FILE_TYPE = "ooTextFile";
	public static final String DEFAULT_OBJECT_CLASS = "TextGrid";
	public String fileType;
	public String objectClass;
	
	//vidiniam naudojimui
	public String originTextGrid;
	
	public Description() {
		fileType = DEFAULT_FILE_TYPE;
		objectClass = DEFAULT_OBJECT_CLASS;
	}
	
	@Override
	public String toString() {
		return "fileType=" + fileType + " objectClass=" + objectClass;
	}

	public String format(Appender appender) {
		return appender.front + "File type = \"" + fileType + "\"" +appender.back + "\n"
				+ appender.front + "Object class = \"" + objectClass + "\"" + appender.back + "\n";
	}

}
