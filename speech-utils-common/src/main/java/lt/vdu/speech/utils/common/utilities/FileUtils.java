package lt.vdu.speech.utils.common.utilities;

import java.io.File;

public class FileUtils {
	
	public static String getNameWithoutExtension(File name) {
		return getNameWithoutExtension(name.getName());
	}
	
	public static String getNameWithoutExtension(String name) {
		if (name == null) {
			return null;
		}
		return name.substring(0, name.lastIndexOf("."));
	}

}
