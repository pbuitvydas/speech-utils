package lt.vdu.speech.utils.common.io.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import lt.vdu.speech.utils.common.dstructures.textgrid.Description;
import lt.vdu.speech.utils.common.dstructures.textgrid.Interval;
import lt.vdu.speech.utils.common.dstructures.textgrid.Item;
import lt.vdu.speech.utils.common.dstructures.textgrid.Range;
import lt.vdu.speech.utils.common.dstructures.textgrid.TextGrid;
import lt.vdu.speech.utils.common.encoding.Encoding;
import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;

public class TextGridReader {
	
	private BufferedReader reader;
	private String lastLine = null;
	private File file;
	
	public TextGridReader(String fileName) {
		try {
			open(fileName);
		} catch (FileNotFoundException e) {
			ExceptionHandler.getInstance().handle(e);
		}
	}
	
	public void open(String fileName) throws FileNotFoundException {
		file = new File(fileName);
		InputStreamReader isr = new InputStreamReader(new FileInputStream(file), Encoding.defaultCharset());
		reader = new BufferedReader(isr);
	}
	
	public void close() throws IOException {
		if (reader != null) {
			reader.close();
		}
	}
	
	public TextGrid readData() throws IOException {
		TextGrid tGrid = new TextGrid();
		tGrid.description = readDescription();
		reader.readLine(); //empy line
		tGrid.range = readRange();
		tGrid.hasTiers = true;
		reader.readLine(); //tiers? <exists> 
		reader.readLine(); //size
		reader.readLine(); //item []:
		lastLine = reader.readLine();
		while (lastLine != null && lastLine.contains("item [")) {
			tGrid.items.add(readItem());
		}
		tGrid.description.originTextGrid = file.getName();
		return tGrid;
	}
	
	protected Description readDescription() throws IOException {
		Description desc = new Description();
		desc.fileType = getString(reader.readLine());
		desc.objectClass = getString(reader.readLine());
		return desc;
	}
	
	protected String getString(String input) {
		int first = input.indexOf('"')+1;
		int last = input.lastIndexOf('"');
		return input.substring(first, last);
	}
	
	protected double getDouble(String input) {
		int start = input.indexOf('=')+1;
		String str = input.substring(start).trim();
		double retVal = Double.parseDouble(str);
		return retVal;
	}
	
	protected float getFloat(String input) {
		int start = input.indexOf('=')+1;
		String str = input.substring(start).trim();
		float retVal = Float.parseFloat(str);
		return retVal;
	}
	
	protected Range readRange() throws IOException {
		Range range = new Range();
		range.xmin = getFloat(reader.readLine());
		range.xmax = getFloat(reader.readLine());
		return range;
	}
	
	protected Interval readInterval() throws IOException {
		Interval retInterval = new Interval();
		retInterval.range = readRange();
		retInterval.text = getString(reader.readLine());
		return retInterval;
	}
	
	protected Item readItem() throws IOException {
		Item item = new Item();
		item.claz = getString(reader.readLine());
		item.name = getString(reader.readLine());
		item.range = readRange();
		reader.readLine(); //skip intervals: size
		while ((lastLine = reader.readLine()) != null && lastLine.contains("intervals [")) {
			item.intervals.add(readInterval());
		}
		return item;
	}

}
