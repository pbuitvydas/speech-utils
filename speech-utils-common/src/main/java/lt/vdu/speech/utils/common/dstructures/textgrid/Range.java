package lt.vdu.speech.utils.common.dstructures.textgrid;

import lt.vdu.speech.utils.common.format.Appender;
import lt.vdu.speech.utils.common.format.Formattable;

/**
 * TextGrid'o intervalui žymėti
 * @author pbuitvydas
 *
 */
public class Range implements Formattable{
	public float xmin;
	public float xmax;
	public Range origRange;
	
	public Range() {
		
	}
	
	public Range(float xmin, float xmax) {
		this.xmin = xmin;
		this.xmax = xmax;
	}

	public String format(Appender appender) {
		return appender.front + "xmin = " + xmin + appender.back + "\n"
				+ appender.front + "xmax = " + xmax + appender.back + "\n";
	}
	
	public float getLength() {
		return xmax - xmin;
	}
	
	public void saveOldRange() {
		this.origRange = new Range();
		this.origRange.xmin = this.xmin;
		this.origRange.xmax = this.xmax;
	}
	
	public Range copyInfo() {
		Range r = new Range();
		r.xmin = this.xmin;
		r.xmax = this.xmax;
		return r;
	}
	
	@Override
	public String toString() {
		return xmin + " -> " + xmax;
	}
}
