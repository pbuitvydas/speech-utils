package lt.vdu.speech.utils.common.dstructures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * HTK žodyno vienetas
 * @author pbuitvydas
 *
 */
public class DictEntry implements Comparable<DictEntry> {
	
/*
	A dictionary for use in HTK has a very simple format. Each line consists of a single word
	pronunciation with format
	WORD [ '['OUTSYM']' ] [PRONPROB] P1 P2 P3 P4 ....
	where WORD represents the word, followed by the optional parameters OUTSYM and PRONPROB, where
	OUTSYM is the symbol to output when that word is recognised (which must be enclosed in square
	brackets, [ and ]) and PRONPROB is the pronunciation probability (0:0 - 1:0). P1, P2, . . . is the
	sequence of phones or HMMs to be used in recognising that word. The output symbol and the
	pronunciation probability are optional. If an output symbol is not specified, the name of the word
	itself is output. If a pronunciation probability is not specified then a default of 1.0 is assumed.
	Empty square brackets, [], can be used to suppress any output when that word is recognised. For
	example, a dictionary might contain
	bit b ih t
	but b ah t
	dog [woof] d ao g
	cat [meow] k ae t
	start [] sil
	end [] sil
*/
	
	
	private String word;
	private String transcript;
	private int entryCount;
	private Float probability;
	private static Map<String, Integer> wordHitCountMap = new HashMap<String, Integer>();
	
	public DictEntry() {
		
	}
	
	public DictEntry(String word, String transcritp) {
		this.word = word;
		this.transcript = transcritp;
		entryCount = 1;
	}
	
	public String getWord() {
		return word;
	}
	
	public void setWord(String word) {
		this.word = word;
	}
	
	public String getTranscript() {
		return transcript;
	}
	
	public void setTranscript(String transcript) {
		this.transcript = transcript;
	}
	
	public String[] getTranscriptArray() {
		return transcript.trim().split("[\t ]");
	}
	
	public List<String> getTranscriptList() {
		return new ArrayList<String>(Arrays.asList(getTranscriptArray()));
	}
	
	public int getEntryCount() {
		return entryCount;
	}

	public void setEntryCount(int entryCount) {
		this.entryCount = entryCount;
	}
	
	public Float getProbability() {
		return probability;
	}

	public void setProbability(Float probability) {
		this.probability = probability;
	}

	public void updateEntryCount() {
		this.entryCount++;
	}
	
	public void updateWordHitCount() {
		if (wordHitCountMap.containsKey(this.word)) {
			int currentCount = wordHitCountMap.get(this.word);
			wordHitCountMap.put(this.word, currentCount + 1);
		} else {
			wordHitCountMap.put(this.word, 1);
		}
	}
	
	public void calculateProbapility() {
		this.probability = (float) entryCount / wordHitCountMap.get(word);
	}

	@Override
	public String toString() {
		if (probability != null) {
			return word  + "    " + probability + "    " + transcript;
		} else {
			return word + "    " + transcript;
		}
		
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof DictEntry) {
			DictEntry other = (DictEntry) obj;
			return this.word.equals(other.word) && this.transcript.equals(other.transcript);
		}
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		int result = 17;
		result += 31 * result + word.hashCode();
		result += 31 * result + transcript.hashCode();
		return result;
	}
	
	public int compareTo(DictEntry arg0) {
		return word.compareTo(arg0.getWord());
	}

}
