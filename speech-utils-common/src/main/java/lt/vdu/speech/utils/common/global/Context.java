package lt.vdu.speech.utils.common.global;

import java.io.File;
import java.util.logging.Filter;
import java.util.logging.Level;
/**
 * TODO reiktu palikt tik bendras
 * @author pbuitvydas
 *
 */
public class Context {
	private static Context ctx;
	
	private File outputDir;
	private String rootDir;
	private FrameConverter frameConverter;
	private String Speaker;
	private Object global;
	
	public static void init() {
		ctx = new Context();
	}
	
	public static Context getInsTance() {
		return ctx;
	}

	public FrameConverter getFrameConverter() {
		return frameConverter;
	}

	public void setFrameConverter(FrameConverter frameConverter) {
		this.frameConverter = frameConverter;
	}

	public File getOutputDir() {
		return outputDir;
	}

	public void setOutputDir(File outputDir) {
		this.outputDir = outputDir;
	}

	public String getSpeaker() {
		return Speaker;
	}

	public void setSpeaker(String speaker) {
		Speaker = speaker;
	}

	public Object getGlobal() {
		return global;
	}

	public void setGlobal(Object global) {
		this.global = global;
	}
}
