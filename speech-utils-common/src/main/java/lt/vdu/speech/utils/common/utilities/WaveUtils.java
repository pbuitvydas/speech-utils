package lt.vdu.speech.utils.common.utilities;

import lt.vdu.speech.utils.common.dstructures.Buffer;
import lt.vdu.speech.utils.common.io.reader.WaveReader;
import lt.vdu.speech.utils.common.io.writer.WaveWriter;
//TODO remove this
public class WaveUtils {
	
	public void openSourceFile(String fileName) {
		
	}
	
	public void openDestinationFile(String fileName) {
		
	}
	
	public void closeSourceFile() {
		
	}
	
	public void closeDestinationFile() {
		
	}

	public void copyAudio(String sourceFileName,
			String destinationFileName, float startSecond, float endSecond) {
		
		try {
			WaveReader reader = new WaveReader(sourceFileName);
			WaveWriter writer = new WaveWriter(destinationFileName, reader.getWaveHeader());
			Buffer readed = reader.read(startSecond, endSecond);
			writer.append(readed);
			Buffer readed2 = reader.read(56.665f, 57.32f);
			writer.append(readed2);
			reader.close();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public static void println(Object o) {
		System.out.println(o);
	}

	public static void print(Object o) {
		System.out.print(o);
	}

}
