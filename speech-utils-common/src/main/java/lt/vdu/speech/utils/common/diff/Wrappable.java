package lt.vdu.speech.utils.common.diff;

import java.util.List;

import lt.vdu.speech.utils.common.io.writer.BaseWriter;
import difflib.Delta;
import difflib.Patch;

public interface Wrappable {
	
	/**
	 * Prieš pradedant wrapinti informacijos pradėjimui
	 * @param patch - skirtumai
	 * @return
	 */
	public String wrapBegin(Patch patch);
	
	public void wrapBegin(BaseWriter<?, ?> writer, Patch patch);
	
	/**
	 * Wrapina kiekvieną skirtumą
	 * @param delta - skirtumas
	 * @return
	 */
	public String wrap(Delta delta);
	
	public void wrapBody(BaseWriter<?, ?> writer, Patch patch);
	
	/**
	 * Wrapinimo užbaigimui
	 * @param patch - skirtumai
	 * @return
	 */
	public String wrapEnd(Patch patch);
	
	public void wrapEnd(BaseWriter<?, ?> writer, Patch patch);

}
