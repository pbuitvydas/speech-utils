package lt.vdu.speech.utils.common.exception;

public class SpeechUtilsException extends Exception {

	private static final long serialVersionUID = -1440856562625769121L;
	
	public SpeechUtilsException(String description) {
		super(description);
	}

}
