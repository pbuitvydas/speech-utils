package lt.vdu.speech.utils.common.global;

import java.math.BigDecimal;

//TODO nereikalingas?? naudoti MlfTime
@Deprecated
public class FrameConverter {
	
	private long coef;
	
	public FrameConverter(long coef) {
		this.coef = coef;
	}
	
	public float framesToSeconds(long frames) {
		BigDecimal bd = new BigDecimal(frames);
		BigDecimal coefBd = new BigDecimal(coef);
		return bd.divide(coefBd).floatValue();
	}

	public long getCoef() {
		return coef;
	}

	public void setCoef(long coef) {
		this.coef = coef;
	}

}
