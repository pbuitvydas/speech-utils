package lt.vdu.speech.utils.common.exception;

public class WordNotFoundException extends SpeechUtilsException {

	private static final long serialVersionUID = -4062696102211633960L;

	public WordNotFoundException(String description) {
		super(description);
	}

}
