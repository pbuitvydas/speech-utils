package lt.vdu.speech.utils.common.format;

/**
 * Formatavaimui pvz į failą
 * @author pbuitvydas
 *
 */
public interface Formattable {
	
	public String format(Appender appender);

}
