package lt.vdu.speech.utils.common.format;

public class Appender {
	public String front = "";
	public String back = "";
	
	public Appender() {
		
	}
	
	public Appender(int nSpaces) {
		for (int i = 0; i < nSpaces; i++) {
			front += " ";
		}
	}
	
}
