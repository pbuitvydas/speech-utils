package lt.vdu.speech.utils.common.config;

import java.util.HashMap;

//TODO pereit prie java.properties configu
/**
 * Konfigūracijos objektas, naudojamas parametų nuskaitymui iš failo ir saugojimui
 * @author pbuitvydas
 *
 */
public class Config {
	
	private HashMap<String, Object> config;
	
	public Config(String fileName) {
		config = ConfigParser.parseConfigFile(fileName);
	}
	
	public String getStringValue(String cfg) {
		return config.get(cfg).toString();
	}
	
	public Object getValue(String cfg) {
		return config.get(cfg);
	}
	
	public Double getDoubleValue(String cfg) {
		return Double.valueOf(getStringValue(cfg));
	}
	
	public Boolean getBooleanValue(String cfg) {
		String str = getStringValue(cfg);
		if ("T".equalsIgnoreCase(str)) {
			str = "true";
		} else if ("F".equalsIgnoreCase(str)) {
			str = "false";
		}
		return Boolean.valueOf(str);
	}
	
	public Integer getIntValue(String cfg) {
		return Integer.valueOf(getStringValue(cfg));
	}

	public HashMap<String, Object> getConfig() {
		return config;
	}
	
}
