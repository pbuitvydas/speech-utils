package lt.vdu.speech.utils.common.io.reader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;

/**
 * String readeris naudojamas skaitymui i stringus
 * standartinis panaudojimas: visu eiluciu nuskaitymas i String list'ą
 * @author pbuitvydas
 *
 */
public class StringReader extends BasicBufferedReader<String> {

	public StringReader(String fileName) {
		super(fileName);
	}
	
	public StringReader(InputStream inputStream) {
		super(inputStream);
	}
	
//	@Override
//	protected BufferedReader createReader() throws Exception {
//		if (getFile() != null) {
//			return new BufferedReader(new FileReader(getFile()));
//		}
//		return new BufferedReader(new InputStreamReader(getStream()));
//	}
	
	@Override
	public List<String> readAsList() {
		List<String> retList = new ArrayList<String>();
		String line;
		try {
			while ((line = readItem()) != null) {
				retList.add(line);
			}
		} catch (Exception e) {
			ExceptionHandler.getInstance().handle(e);
		}
		return retList;
	}
	
	public String dumpToString() {
		StringBuilder sb = new StringBuilder();
		String line;
		try {
			while ((line = readItem()) != null) {
				sb.append(line + "\n");
			}
		} catch (Exception e) {
			ExceptionHandler.getInstance().handle(e);
		}
		return sb.toString();
	}
	
	@Override
	public String readItem() throws Exception {
		return readAsString();
	}

}
