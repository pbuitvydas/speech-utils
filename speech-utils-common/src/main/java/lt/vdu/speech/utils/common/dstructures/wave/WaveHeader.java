package lt.vdu.speech.utils.common.dstructures.wave;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class WaveHeader {
	public static int HEADER_LEN = 44;
	private int frameSize;
	private float frameRate;
	private byte[] headerData = new byte[HEADER_LEN];
	
	
	public void readHeader(String fileName) throws IOException {
		FileInputStream fis = new FileInputStream(new File(fileName));
		readHeader(fis);
	}
	
	public void readHeader(InputStream inStream) throws IOException {
		inStream.read(headerData);
	}
	
	public byte[] getHeaderData() {
		return headerData;
	}

	public int getFrameSize() {
		return frameSize;
	}

	public void setFrameSize(int frameSize) {
		this.frameSize = frameSize;
	}

	public float getFrameRate() {
		return frameRate;
	}

	public void setFrameRate(float frameRate) {
		this.frameRate = frameRate;
	}
}
