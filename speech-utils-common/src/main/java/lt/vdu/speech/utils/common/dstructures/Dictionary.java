package lt.vdu.speech.utils.common.dstructures;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lt.vdu.speech.utils.common.exception.WordNotFoundException;
import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;

import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.MultiValueMap;

public class Dictionary {
	
	private MultiMap dictEntries;
	private Map<String, Integer> wordCountMap;
	private int entryCount = 0;
	
	public Dictionary() {
		dictEntries = new MultiValueMap();
		wordCountMap = new HashMap<String, Integer>();
	}
	
	/**
	 * gauti žodyno įrašuas pgal žodį
	 * @param word
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<DictEntry> getEntries(String word) throws WordNotFoundException {
		Collection<DictEntry> coll = (Collection<DictEntry>) dictEntries.get(word);
		if (coll == null) {
			throw new WordNotFoundException("word " + word + " not found in dictionary");
		}
		if (coll instanceof List) {
			return (List<DictEntry>) coll;
		}
		return new ArrayList<DictEntry>(coll);
	}
	
	/**
	 * įterpia žodyno įrašą
	 * @param entry
	 * @return
	 */
	public DictEntry put(DictEntry entry) {
		entry.updateWordHitCount();
		if (hasWord(entry.getWord())) {
			DictEntry e = getByDictEntry(entry);
			if (e != null) {
				// toks tarimas jau yra
				e.updateEntryCount();
				return e;
			}
		}
		entryCount++;
		return (DictEntry) dictEntries.put(entry.getWord(), entry);
	}
	
	public void remove(DictEntry entry) {
		dictEntries.remove(entry.getWord(), entry);
		entryCount--;
	}
	
	public boolean hasWord(String word) {
		return getWords().contains(word);
	}
	
	public DictEntry getByDictEntry(DictEntry entry) {
		List<DictEntry> entrylList;
		try {
			entrylList = getEntries(entry.getWord());
			for (DictEntry e : entrylList) {
				if (e.equals(entry)) {
					return e;
				}
			}
		} catch (WordNotFoundException e1) {
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public Set<String> getWords() {
		return dictEntries.keySet();
	}
	
	public void generateProbabilities() {
		try {
			for (String word : getWords()) {
				for (DictEntry entry : getEntries(word)) {
					entry.calculateProbapility();
				}
			}
		} catch (WordNotFoundException e) {
			ExceptionHandler.getInstance().handle(e);
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public int getEntryCount() {
		return entryCount;
	}
	
	public void clear() {
		entryCount = 0;
		dictEntries.clear();
	}
	

}
