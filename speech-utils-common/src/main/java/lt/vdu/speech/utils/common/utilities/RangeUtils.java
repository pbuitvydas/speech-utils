package lt.vdu.speech.utils.common.utilities;

import lt.vdu.speech.utils.common.dstructures.textgrid.Range;
import lt.vdu.speech.utils.common.global.Context;
import lt.vdu.speech.utils.common.global.FrameConverter;

public class RangeUtils {
	
	/**
	 * Suformuoja Range objketa su is nurodytu freimu, convertavimas atliakmas su is konteksto pasiimamu FrameConverteriu
	 * @param xmin
	 * @param xmax
	 * @return
	 */
	public static Range rangeFromFrames(long xmin, long xmax) {
		FrameConverter frameConverter = Context.getInsTance().getFrameConverter();
		return new Range(frameConverter.framesToSeconds(xmin), frameConverter.framesToSeconds(xmax));
	}

}
