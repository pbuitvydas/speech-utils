package lt.vdu.speech.utils.common.dstructures.textgrid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TextGrid {
	
	public Description description;
	public Range range;
	public boolean hasTiers;
	public List<Item> items;
	public static final List<String> TEE_MODELS = new ArrayList<String>() {

		private static final long serialVersionUID = 1L;

		{
			add("sp");
			add("sil");
			add("sill");
		}
	};
	
	public TextGrid() {
		description = new Description();
		range = new Range();
		items = new ArrayList<Item>();
	}
	
	/**
	 * I textGrida ideda viena juosta (tier'a)
	 * @param item suformuota jousta
	 */
	public void addItem(Item item) {
		items.add(item);
	}
	
	public TextGrid subGrid(float xmin, float xmax) {
		TextGrid subGrid = new TextGrid();
		subGrid.description = this.description;
		subGrid.hasTiers = this.hasTiers;
		subGrid.range = this.range;
		subGrid.items = copyBaseIntervals(this.items.size()-1);
		for (int i = 0; i < this.items.size(); i++) {
			findIntervals(subGrid, i, xmin, xmax);
		}
		updateRanges(subGrid);
		return subGrid;
	}
	
	public TextGrid subGrid(String... words) {
		TextGrid subGrid = new TextGrid();
		List<String> wordList = Arrays.asList(words);
		subGrid.description = this.description;
		subGrid.hasTiers = this.hasTiers;
		subGrid.range = this.range;
		subGrid.items = new ItemList();
		Item item = getItem("zodziai");
		int itemIndx = this.items.indexOf(item);
		subGrid.items = copyBaseIntervals(itemIndx);
		for (int j = 0; j <= itemIndx; j++) {
			for (int i = 0; i < item.intervals.size(); i++) {
				Interval inter = item.intervals.get(i);
				if (wordList.contains(inter.text)) {
					Interval teeFront = item.intervals.get(i-1);
					Interval teeBack = item.intervals.get(i+1);//TODO pries ir po zodis
					Interval start = TEE_MODELS.contains(teeFront.text) ? teeFront : inter;
					Interval stop = TEE_MODELS.contains(teeBack.text) ? teeBack : inter;
					findIntervals(subGrid, j, start.range.xmin,
							stop.range.xmax);
				}
			}
		}

		updateRanges(subGrid);
		return subGrid;
	}
	
	public void updateRanges(TextGrid grid) {
		float max = 0;
		for (Item i : grid.items) {
			int index = i.intervals.size()-1;
			if (index < 0) {
				continue;
			}
			max = i.intervals.get(index).range.xmax;
			i.range.xmax = max;
		}
		grid.range.xmax = max;
	}
	
	public List<Item> copyBaseIntervals(int untilIndex) {
		List<Item> copiedBareItems = new ItemList();
		for (int i = 0; i <= untilIndex; i++) {
			Item copiedItem = this.items.get(i).copyBaseInfo();
			copiedItem.intervals = new IntervalList();
			copiedBareItems.add(copiedItem);
		}
		return copiedBareItems;
	}
	
	public Item getItem(String name) {
		for (Item i : this.items) {
			if (i.name.equals(name)) {
				return i;
			}
		}
		return null;
	}
	
	protected List<Interval> findIntervals(TextGrid fillGrid, int itemIndx, double xmin, double xmax) {
		Item item = this.items.get(itemIndx);
		List<Interval> intervals = fillGrid.items.get(itemIndx).intervals;
		for (int i = 0; i < item.intervals.size(); i++) {
			if (item.intervals.get(i).range.xmin == xmin) {
				for (; i < item.intervals.size(); i++) {
					if (item.intervals.get(i).range.xmax <= xmax) {
						intervals.add(item.intervals.get(i));
					}
					else {
						break;
					}
				}
				//todo sustapdyt cikla
			}
		}
		return intervals;
		
	}
	
	public void appendTextGrid(TextGrid textGrid) throws Exception {
		//TODO jei apendini save pati referencai pasiupdatina reiketu klonuoti
		for (int i = 0; i < this.items.size(); i++) {
			Item gridItem = this.items.get(i);
			if (!(gridItem.intervals instanceof IntervalList)) {
				throw new Exception("intervalai turi būti IntervalList tipo, dabar yra" + gridItem.intervals.getClass());
			}
			for (Interval inter : textGrid.items.get(i).intervals) {
				gridItem.intervals.add(inter);
			}
		}
		updateRanges(this);
	}
	
	/**
	 * Darant subGrid kad perskaiciuotu laikus
	 * @author pbuitvydas
	 *
	 */
	public class IntervalList extends ArrayList<Interval> {
		
		private static final long serialVersionUID = 4057367247471690115L;
		
		@Override
		public boolean add(Interval e) {
			if (this.size() == 0) {
				e.range.saveOldRange();
				e.range.xmax = e.range.getLength();
				e.range.xmin = 0;
			}
			else {
				float lastMax = this.get(this.size()-1).range.xmax;
				e.range.saveOldRange();
				e.range.xmax = lastMax + e.range.getLength();
				e.range.xmin = lastMax;
			}
			return super.add(e);
		}
		
	}
	
	public class ItemList extends ArrayList<Item> {

		private static final long serialVersionUID = 7055474943107138225L;
		
		@Override
		public boolean add(Item e) {
			return super.add(e);
		}
		
	}

}
