package lt.vdu.speech.utils.common.config;

import java.util.HashMap;
import java.util.List;

import lt.vdu.speech.utils.common.io.reader.StringReader;

//TODO pereit prie java.properties configu
//http://commons.apache.org/configuration/userguide/user_guide.html
public class ConfigParser {
	
	
	public static HashMap<String, Object> parseConfigFile(String file) {
		HashMap<String, Object> retVal = new HashMap<String, Object>();
		StringReader bsr = new StringReader(file);
		List<String> lst = bsr.readAsList();
		for (String str : lst) {
			String[] splits = str.split("=");
			if (splits.length == 2) {
				retVal.put(splits[0].trim(), splits[1].trim());
			}
		}
		return retVal;
	}

}
