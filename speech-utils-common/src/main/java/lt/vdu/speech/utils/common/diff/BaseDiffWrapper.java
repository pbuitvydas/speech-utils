package lt.vdu.speech.utils.common.diff;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;
import lt.vdu.speech.utils.common.io.writer.BaseWriter;
import difflib.Delta;
import difflib.Patch;

/**
 * Bazinis skirtumų wrapperis, nieko nedaro, o tik pateikia apibendrintus metodus
 * naudojamas užvrapinti patch'ą į kokį nors formatą
 * @author pbuitvydas
 *
 */
public abstract class BaseDiffWrapper<T> implements Wrappable {
	
	private static final Logger log = LoggerFactory.getLogger(BaseDiffWrapper.class);
	
	private List<T> original;
	private List<T> revised;

	public List<T> getOriginal() {
		return original;
	}
 
	public void setOriginal(List<T> original) {
		this.original = original;
	}

	public List<T> getRevised() {
		return revised;
	}

	public void setRevised(List<T> revised) {
		this.revised = revised;
	}
	
	public T getOriginal(int idx) {
		return getItem(getOriginal(), idx);
	}
	
	public T getRevised(int idx) {
		return getItem(getRevised(), idx);
	}
	
	private T getItem(List<T> list, int idx) {
		if (list == null) {
			log.warn("Item list is empty, maybe not initialised");
			return null;
		}
		if (idx < 0 || idx > (list.size()-1)) {
			return null;
		}
		return list.get(idx);
	}
	
	public void wrapBegin(BaseWriter<?, ?> writer, Patch patch) {
		try {
			writer.writeString(wrapBegin(patch));
		} catch (Exception e) {
			ExceptionHandler.getInstance().handle(e);
		}
	}
	
	public void wrapBody(BaseWriter<?, ?> writer, Patch patch) {
		wrapBody(writer, patch.getDeltas());
	}
	
	protected void wrapBody(BaseWriter<?, ?> writer, List<Delta> deltaList) {
		try {
			for (int i = 0; i < deltaList.size(); i++) {
				writer.writeString(wrap(deltaList.get(i)) + "\n");
			}
		} catch (Exception e) {
			ExceptionHandler.getInstance().handle(e);
		}
	}
	
	public void wrapEnd(BaseWriter<?, ?> writer, Patch patch) {
		try {
			writer.writeString(wrapEnd(patch));
		} catch (Exception e) {
			ExceptionHandler.getInstance().handle(e);
		}
	}

}
