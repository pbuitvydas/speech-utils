package lt.vdu.speech.utils.common.diff;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;
import lt.vdu.speech.utils.common.io.reader.BaseReader;
import lt.vdu.speech.utils.common.io.writer.BaseWriter;

import difflib.DiffUtils;
import difflib.Patch;
import difflib.PatchFailedException;

/**
 * Bendrinė palyginimų klasė
 * @author pbuitvydas
 *
 * @param <T> - lyginamasis vienetas
 */
public class CommonDiff<T> {
	
	private static final Logger log = LoggerFactory.getLogger(CommonDiff.class);
	
	private Patch patch;
	private List<T> original;
	private List<T> revised;
	
	/**
	 * metodas atlieka palyginimus
	 * @param original
	 * @param revised
	 */
	public void diff(List<T> original, List<T> revised) {
		this.original = original;
		this.revised = revised;
		this.patch = DiffUtils.diff(original, revised);
	}
	
	public void diff(BaseReader<?, T> original, BaseReader<?, T> revised) {
		try {
			log.info("Reading original content...");
			List<T> originalList = original.readAsList();
			log.info("Reading revised content...");
			List<T> revisedList = revised.readAsList();
			log.info("Differencing content...");
			diff(originalList, revisedList);
		} catch (Exception e) {
			ExceptionHandler.getInstance().handle(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<T> patch() {
		List<T> retList = null;
		try {
			retList = (List<T>) DiffUtils.patch(original, patch);
		} catch (PatchFailedException e) {
			ExceptionHandler.getInstance().handle(e);
		}
		return retList;
	}
	
	public void wrapResult(BaseWriter<?, T> writer, BaseDiffWrapper<T> wrapper) {
		if (patch == null) {
			return;//TODO throw exception
		}
		wrapper.setOriginal(original);
		wrapper.setRevised(revised);
		wrapper.wrapBegin(writer, patch);
		wrapper.wrapBody(writer, patch);
		wrapper.wrapEnd(writer, patch);
	}

	public Patch getPatch() {
		return patch;
	}

	public List<T> getOriginal() {
		return original;
	}

	public List<T> getRevised() {
		return revised;
	}
}
