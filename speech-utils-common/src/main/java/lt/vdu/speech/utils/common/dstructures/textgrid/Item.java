package lt.vdu.speech.utils.common.dstructures.textgrid;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lt.vdu.speech.utils.common.format.Appender;
import lt.vdu.speech.utils.common.format.Formattable;

/**
 * TextGrid'o bazinis vienetas
 * @author pbuitvydas
 *
 */
public class Item implements Formattable{
	
	public static final Logger log = LoggerFactory.getLogger(Item.class);
	
	public String claz;
	public String name;
	public Range range;
	public List<Interval> intervals;
	
	public Item() {
		intervals = new ArrayList<Interval>();
		range = new Range();
	}
	
	/**
	 * I juostą įdeda vieną vienetą su pavadinimu ir range'u
	 * @param interval - intervalas kurį norima įdėti
	 * @return - true jei pavyko ideti
	 */
	public boolean addInterval(Interval interval) {
		if (intervals.size() > 0 ) {
			Interval last = intervals.get(intervals.size() -1); //jei praaito intervalo pabaiga didesne uz naujo pradzia(intervalas ulipa vienas ant kito)
			if (last.range.xmax > interval.range.xmin) {
				log.error("!!! intervalas ulipa vienas ant kito " + last.range.xmax + " > " + interval.range.xmin );
			}
		}
		if (interval.range.xmin > interval.range.xmax) {
			log.error("!!! nelogiskas intervalas " + interval.range.xmin + " > " + interval.range.xmax );
		}
		return intervals.add(interval);
	}
	
	public Item copyBaseInfo() {
		Item item = new Item();
		item.claz = this.claz;
		item.name = this.name;
		item.range = this.range.copyInfo();
		return item;
	}


	public String format(Appender appender) {
		return appender.front + "class = \"" + claz + "\"\n" + appender.back
				+ appender.front + "name = \"" + name + "\"\n" + appender.back 
				+ range.format(appender)
				+ appender.front + "intervals: size = " + intervals.size() + appender.back + "\n";
		
	}

}
