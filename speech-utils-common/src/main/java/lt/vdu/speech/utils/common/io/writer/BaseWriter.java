package lt.vdu.speech.utils.common.io.writer;

import java.io.File;
import java.util.List;

import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;

/**
 * Bazinis writerio wraperis
 * @author pbuitvydas
 *
 * @param <T> - writeris kuri wrappinam
 * @param <TT> - duomenys kuriuos rasysim
 */
public abstract class BaseWriter<T, TT> {
	
	protected T writer;
	private File file;
	
	public BaseWriter() {
		try {
			writer = createWriter();
		} catch (Exception e) {
			ExceptionHandler.getInstance().handle(e);
		}
	}
	
	public BaseWriter(String fileName) {
		this(new File(fileName));
	}
	
	public BaseWriter(File file) {
		this.file = file;
		try {
			writer = createWriter();
		} catch (Exception e) {
			ExceptionHandler.getInstance().handle(e);
		}
	}
	
	protected abstract T createWriter() throws Exception;
	protected abstract void closeWriter() throws Exception;
	
	public void close() {
		try {
			closeWriter();
		} catch (Exception e) {
			ExceptionHandler.getInstance().handle(e);
		}
	}
	
	public void write(TT item) throws Exception {
		
	}
	
	public void write(List<TT> items) throws Exception {
		
	}
	
	public void writeString(String str) throws Exception {
		
	}
	
	public File getFile() {
		return file;
	}
	
	protected T getWriter() {
		return writer;
	}
	

}
