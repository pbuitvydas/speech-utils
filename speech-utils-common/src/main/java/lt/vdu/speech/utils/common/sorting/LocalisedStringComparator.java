package lt.vdu.speech.utils.common.sorting;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

public class LocalisedStringComparator implements Comparator<String> {
	
	private Collator collator;
	
	public LocalisedStringComparator() {
		collator = Collator.getInstance(new Locale("lt"));
	}

	@Override
	public int compare(String strA, String strB) {
		return collator.compare(strA, strB);
	}

}
