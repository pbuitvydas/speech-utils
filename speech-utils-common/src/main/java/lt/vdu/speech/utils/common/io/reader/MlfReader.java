package lt.vdu.speech.utils.common.io.reader;

import java.io.File;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;

import lt.vdu.speech.utils.common.dstructures.Mlf;
import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;

/**
 * <pre>
 * <    left    >< middle ><              right                   > <br>
 * [start [end] ]   name   [score] { auxname [auxscore] } [comment]
 * </pre> 
 * @author pbuitvydas
 *
 */
public class MlfReader extends BasicBufferedReader<Mlf> {
	
	private String labName;
	
	public MlfReader(String fileName) {
		super(fileName);
	}
	
	public MlfReader(InputStream inputStream) {
		super(inputStream);
	}
	
	@Override
	public List<Mlf> readAsList() {
		List<Mlf> retList = new ArrayList<Mlf>();
		try {
			Mlf mlf;
			while ((mlf = readItem()) != null) {
				retList.add(mlf);
			}
		} catch (Exception e) {
			ExceptionHandler.getInstance().handle(e);
		}
		
		return retList;
	}
	
	@Override
	public Mlf readItem() throws Exception {
		String str = readAsString();
		if (str != null) {
			str = str.trim();
			String middle = getItem(str);
			if (middle.contains(".lab") || middle.contains(".rec")) {
				labName = middle.replace(".rec", ".lab").replace("\"", "");
			}
			Mlf mlf = new Mlf(str, middle);
			mlf.setLabName(labName);
			String left = getLeft(str, middle);
			String right = getRight(str, middle);
			mlf.setRight(right);
			mlf.setLeft(left);
			parseTime(left, mlf);
			parseAuxilary(right, mlf);
			return mlf;
		}
		return null;
	}
	
	public String getLeft(String data, String middle) {
		int idx = data.indexOf(middle);
		if (idx > 0) {
			return data.substring(0, idx);
		} else {
			return null;
		}
	}
	
	public String getRight(String data, String middle) {
		int idx = data.indexOf(middle);
		String retVal = null;
		if (idx != -1 && idx < data.length() -1) {
			retVal = data.substring(idx + middle.length());
			if (retVal.length() == 0) {
				retVal = null;
			}
		}
		return retVal;
	}
	
	/**
	 * Kadangi mlf'e minimaliai turi būti label'io vardas (neskaitinė reikšmė), todėl iš duotos eilutės
	 * bandoma ištraukti label'io pavadinimą
	 * @param data
	 * @return
	 * @throws ParseException
	 */
	protected String getItem(String data) throws ParseException {
		if (data == null) {
			return null;
		}
		String[] splits = data.split("[ \t]");
		for (String str : splits) {
			//TODO atsisakyti, perteklrine priklausomybe
			if (!NumberUtils.isNumber(str)) {
				return str;
			}
		}
		throw new ParseException("Mandatory label name not found for given string: " + data, 0);
	}
	
	/**
	 * duotai eilutei ištraukia papildomus duomenis pvz.: score
	 * score gali ir nebūt jei malformed mlf'as ir sukeisti stulpeliai <br>
	 * <pre>
	 * [score] { auxname [auxscore] } [comment]
	 * <pre>
	 * 
	 * @param data
	 * @param mlf
	 */
	protected void parseAuxilary(String data, Mlf mlf) {
		if (data == null) {
			return;
		}
		String[] splits = data.trim().split("[ \t]");
		if (splits.length > 0 && NumberUtils.isNumber(splits[0])) {
			mlf.setProba(Float.valueOf(splits[0]));
		}
	}
	
	/**
	 * Duotai eilutei ištraukia laiką, papildomai patikrinama ar nėra malformed mlf'as turintis
	 * sukeistą score stulpelį <br>
	 * <pre>
	 * [start [end] ]
	 * </pre>
	 * @param data
	 * @param mlf
	 * @throws ParseException
	 */
	protected void parseTime(String data, Mlf mlf) throws ParseException {
		if (data == null) {
			return;
		}
		String[] splits = data.trim().split("[ \t]");
		if (splits.length == 2) {
			mlf.setStart2(Long.valueOf(splits[0]));
			mlf.setStop2(Long.valueOf(splits[1]));
		} else if(splits.length == 3) {
			mlf.setStart2(Long.valueOf(splits[0]));
			mlf.setStop2(Long.valueOf(splits[1]));
			mlf.setProba(Float.valueOf(splits[2]));
		} else {
			throw new ParseException("Error while processing line " + mlf 
					+ "\nMessage: Could not parse time from string - \"" + data + "\"", 0);
		}
	}
	
}
