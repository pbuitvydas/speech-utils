package lt.vdu.speech.utils.common.utilities;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.Collection;
import java.util.List;

/**
 * Utility klasė darbui su java klasėmis
 * @author pbuitvydas
 *
 */
public class ClassUtils {
	
	/**
	 * Gauti resursą per nurodytos klasės class loader'į
	 * @param clazz
	 * @param resourceName - resurso pavadinimas/arba pilnas kelias
	 * @return
	 */
	public static File getResource(Class<?> clazz, String resourceName) {
		URL url = clazz.getClassLoader().getResource(resourceName);
		if (url == null) {
			return null;
		}
		return new File(url.getFile());
	}
	
	/**
	 * automatiškai gauti resursą pagal nurodytą pavadinimą
	 * jei pagal nurodytą pavadinimą nerandama sulipdomas nurodytos klasės package kelias
	 * ir ieškoma ten
	 * @param clazz
	 * @param resourceName
	 * @return
	 */
	public static File getResourceAuto(Class<?> clazz, String resourceName) {
		File file = getResource(clazz, resourceName);
		if (file == null) {
			String path = clazz.getPackage().getName();
			path = path.replace('.', '/');
			path += "/" + resourceName;
			file = getResource(clazz, path);
		}
		return file;
	}
	
	public static InputStream getResourceAsStream(Class<?> clazz, String resourceName) {
		return clazz.getClassLoader().getResourceAsStream(resourceName);
	}
	
	public static InputStream getResourceAsStreamAuto(Class<?> clazz, String resourceName) {
		InputStream inSetream = getResourceAsStream(clazz, resourceName);
		if (inSetream == null) {
			String path = clazz.getPackage().getName();
			path = path.replace('.', '/');
			path += "/" + resourceName;
			inSetream = getResourceAsStream(clazz, path);
		}
		return inSetream;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> List<T> unwild(List<?> wildList) {
		return (List<T>) wildList;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> Collection<T> unwild(Collection<?> wildCollection) {
		return (Collection<T>) wildCollection;
	}

}
