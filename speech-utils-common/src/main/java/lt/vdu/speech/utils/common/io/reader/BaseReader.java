package lt.vdu.speech.utils.common.io.reader;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;

/**
 * Bazinis Readerio wraperis, turintis metodus nuskaitymui,
 * error handliniumui
 * 
 * @author pbuitvydas
 *
 * @param <T> - Readeris kurį wrapinam
 * @param <TT> - Struktūra į kurią pus skaitomi duomenys
 */
public abstract class BaseReader<T, TT> {
	
	protected T reader;
	
	private File file;
	private InputStream stream;
	
	public BaseReader(String fileName) {
		this(new File(fileName));
	}
	
	public BaseReader(InputStream inputStream) {
		this.stream = inputStream;
		try {
			this.reader = createReader(); 
		}
		catch (Exception e) {
			ExceptionHandler.getInstance().handle(e);
		}
	}
	
	public BaseReader(File file) {
		this.file = file;
		try {
			this.reader = createReader(); 
		}
		catch (Exception e) {
			ExceptionHandler.getInstance().handle(e);
		}
	}
	
	public void close() {
		try {
			closeReader();
		} catch (Exception e) {
			ExceptionHandler.getInstance().handle(e);
		}
		reader = null;
	}
	
	protected abstract T createReader() throws Exception;
	
	protected void closeReader() throws Exception {
		
	}
	
	public TT readItem() throws Exception {
		throw new UnsupportedOperationException("not implemented");
	}
	
	public TT read(long start, int len) {
		throw new UnsupportedOperationException("not implemented");
	}
	
	public List<TT> readAsList() throws Exception {
		throw new UnsupportedOperationException("not implemented");
	}
	
	public String readAsString() throws Exception {
		throw new UnsupportedOperationException("not implemented");
	}
	
	public File getFile() {
		return file;
	}

	public InputStream getStream() {
		return stream;
	}

	public void setStream(InputStream stream) {
		this.stream = stream;
	}

}
