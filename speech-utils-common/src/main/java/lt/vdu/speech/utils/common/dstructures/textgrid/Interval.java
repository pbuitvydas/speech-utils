package lt.vdu.speech.utils.common.dstructures.textgrid;

import lt.vdu.speech.utils.common.format.Appender;
import lt.vdu.speech.utils.common.format.Formattable;

/**
 * 
 * @author pbuitvydas
 *
 */
public class Interval implements Formattable {
	public Range range;
	public String text;
	
	public Interval() {
		range = new Range();
	}
	
	public String format(Appender appender) {
		return range.format(appender) 
				+ appender.front + "text = \"" + text + "\"" + appender.back + "\n";
	}
	
	@Override
	public String toString() {
		return text + ", Range: " + range.toString();
	}
}
