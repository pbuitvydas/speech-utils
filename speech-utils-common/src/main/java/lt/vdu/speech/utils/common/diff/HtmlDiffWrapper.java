package lt.vdu.speech.utils.common.diff;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import lt.vdu.speech.utils.common.io.writer.BaseWriter;
import difflib.Chunk;
import difflib.Delta;
import difflib.Delta.TYPE;
import difflib.Patch;

/**
 * HTML skirtumų wrapperis, wrapina skirtumus į skaitomą html kodą
 * @author pbuitvydas
 *
 */
public class HtmlDiffWrapper<T> extends BaseDiffWrapper<T> {
	
	private static final Map<TYPE, String> CHANGES_COLOR_MAP = new HashMap<Delta.TYPE, String>() {
		{
			put(TYPE.CHANGE, "background-color:khaki");
			put(TYPE.DELETE, "background-color:pink");
			put(TYPE.INSERT, "background-color:green");
		}
	};

	public String wrapBegin(Patch patch) {
		return htmlBegin(patch) + "\n" 
				+ head(patch) + "\n"
				+ bodyBegin(patch) + "\n"
				+ tableBegin(patch);
	}
	
	public String htmlBegin(Patch patch) {
		return "<html>";
	}
	
	public String htmlEnd(Patch patch) {
		return "</html>";
	}
	
	public String bodyBegin(Patch patch) {
		return "<body>";
	}
	
	public String bodyEnd(Patch patch) {
		return "</body>";
	}
	
	public String head(Patch patch) {
		return headBegin(patch)
				+ headContent(patch)
				+ headEnd(patch);
	}
	
	public String headBegin(Patch patch) {
		return "<head>";
	}
	
	public String headEnd(Patch patch) {
		return "</head>";
	}
	
	public String headContent(Patch patch) {
		return "";
	}
	
	public String tableBegin(Patch patch) {
		return "<table style='border:1px solid'>";
	}
	
	public String tableEnd(Patch patch) {
		return "</table>";
	}
	
	public String wrap(Delta delta) {
		String str = "<tr>";
		String value = null;
		if (delta != null) {
			value = getLValue(getOriginal(), delta.getOriginal(), delta.getType());
			str += td(value, null);
			str += td("&nbsp;", "");
			value = getRValue(getRevised(), delta.getRevised(), delta.getType());
			str += td(value, null);
		}
		str += "</tr>";
		return str;
	}

	/**
	 * Pagal pakeitimo tipą gražina spalvos stilių
	 * @param type - pakeitimo tipas
	 * @return
	 */
	public String getColor(TYPE type) {
		if (type == null) {
			return "";
		}
		return CHANGES_COLOR_MAP.get(type);
	}
	
	/**
	 * Bendras lentelė celės formavimo metodas
	 * @param value - celės turinys
	 * @param style - celės stilius (style atributas)
	 * @return
	 */
	public String td(String value, String style) {
		return "<td style=\"" + (style != null ? style : "") + "\">" + (value != null ? value : "") + "</td>";  
	}
	
	/**
	 * Iš pakeitimo gauti turinį tai yra pakeistą konkrečią reikšmę dešinės pusės
	 * @param chunk - pakeitimo gabalas
	 * @param type - pakeitimo tipas
	 * @return
	 */
	public String getRValue(List<T> contentList, Chunk chunk, TYPE type) {
		return getValue(contentList, chunk, type);
	}
	
	/**
	 * Iš pakeitimo gauti turinį tai yra pakeistą konkrečią reikšmę kairės pusės
	 * @param chunk - pakeitimo gabalas
	 * @param type - pakeitimo tipas
	 * @return
	 */
	public String getLValue(List<T> contentList, Chunk chunk, TYPE type) {
		return getValue(contentList, chunk, type);
	}
	
	/**
	 * Iš pakeitimo gauti turinį tai yra pakeistą konkrečią reikšmę universalus metodas
	 * (nediferencijuoja kuris originalus kuris revised)
	 * @param chunk - pakeitimo gabalas
	 * @param type - pakeitimo tipas
	 * @return
	 */
	public String getValue(List<T> contentList, Chunk chunk, TYPE type) {
		if (chunk == null) {
			return "";
		}
		if (chunk.getLines() == null) {
			return "";
		}
		Iterator<?> it = chunk.getLines().iterator();
		if (it.hasNext()) {
			return it.next().toString();
		}
		return "";
	}
	
	public String wrapEnd(Patch patch) {
		return tableEnd(patch) + "\n"
				+ bodyEnd(patch) + "\n"
				+ htmlEnd(patch) + "\n";
	}

}
