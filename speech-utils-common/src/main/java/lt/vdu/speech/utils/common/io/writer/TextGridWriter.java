package lt.vdu.speech.utils.common.io.writer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

import lt.vdu.speech.utils.common.dstructures.textgrid.Interval;
import lt.vdu.speech.utils.common.dstructures.textgrid.Item;
import lt.vdu.speech.utils.common.dstructures.textgrid.TextGrid;
import lt.vdu.speech.utils.common.encoding.Encoding;
import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;
import lt.vdu.speech.utils.common.format.Appender;

public class TextGridWriter {
	
	private BufferedWriter writer;
	private Appender oneTab, twoTab, threeTab;
	
	public TextGridWriter() {
		oneTab = new Appender(4);
		twoTab = new Appender(8);
		threeTab = new Appender(12);
	}
	
	public TextGridWriter(String fileName) {
		this();
		try {
			open(fileName);
		} catch (IOException e) {
			ExceptionHandler.getInstance().handle(e);
		}
	}
	
	public void open(String fileName) throws IOException {
		OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(fileName), Encoding.defaultCharset());
		writer = new BufferedWriter(osw);
	}
	
	public void close() throws IOException {
		if (writer != null) {
			writer.close();
		}
	}
	
	public void writeData(TextGrid data) throws IOException {
		writer.write(data.description.format(new Appender()));
		writer.write("\n");
		writer.write(data.range.format(new Appender()));
		writer.write("tiers? <exists> \n");
		writer.write("size = " + data.items.size() + "\n");
		writer.write("item []: \n");
		int i = 1, j = 1;
		for (Item item : data.items) {
			writer.write(oneTab.front + "item [" + i++ + "]:\n");
			writer.write(item.format(twoTab));
			for (Interval interval : item.intervals) {
				writer.write(twoTab.front + "intervals [" + j++ + "]:\n");
				writer.write(interval.format(threeTab));
			}
			j = 1;
		}
	}

}
