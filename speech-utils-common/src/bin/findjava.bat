:: check if java exists in JAVA_HOME
if "%JAVA_HOME%"=="" goto checkPATH
if not exist "%JAVA_HOME%\bin\java.exe" goto checkPATH
set _JAVACMD=%JAVA_HOME%\bin\java.exe
goto end

:: if not in JAVA_HOME search for java executable in PATH environment variable
:checkPATH
for %%X in (java.exe) do (set FOUND=%%~$PATH:X)
if not defined FOUND goto checkRegistry
set _JAVACMD=java.exe
goto end

:: if not in path search in registry
:checkRegistry
FOR /F "skip=2 tokens=2*" %%A IN 
    ('REG QUERY "HKLM\Software\JavaSoft\Java Runtime Environment" 
    /v CurrentVersion') DO set CurVer=%%B

FOR /F "skip=2 tokens=2*" %%A IN 
    ('REG QUERY "HKLM\Software\JavaSoft\Java Runtime Environment\%CurVer%" 
    /v JavaHome') DO set _JAVACMD=%%B\bin\java.exe

:end