package lt.vdu.speech.utils.common.config;

import java.net.URL;
import java.util.HashMap;

import junit.framework.TestCase;

public class ConfigParserTest extends TestCase {
	
	private String fileName;
	
	@Override
	protected void setUp() throws Exception {
		URL url = ClassLoader.getSystemResource("lt/vdu/speech/utils/common/config/settings.cfg");
		fileName = url.getFile();
		super.setUp();
	}

	public void testParseConfigFile() {
		try {
			Object obj = ConfigParser.parseConfigFile(fileName);
			if (obj instanceof HashMap) {
				HashMap<String, Object> map = (HashMap<String, Object>) obj;
				assertEquals(map.size(), 7);
			}
			assertNotNull(obj);
		} catch (Exception e) {
			fail("exception thrown at: " + getName() + "\n" + e.getMessage());
		}
	}

}
