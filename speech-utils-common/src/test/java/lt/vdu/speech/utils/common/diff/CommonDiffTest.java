package lt.vdu.speech.utils.common.diff;

import java.util.List;

import difflib.DiffUtils;
import difflib.Patch;

import junit.framework.TestCase;
import lt.vdu.speech.utils.common.io.reader.StringReader;
import lt.vdu.speech.utils.common.io.writer.BaseWriter;
import lt.vdu.speech.utils.common.io.writer.StringWriterStdOut;
import lt.vdu.speech.utils.common.utilities.TestUtils;

public class CommonDiffTest extends TestCase {
	
	private static final String PLAIN_ORIGINAL_FILE = "plain-original.txt";
	private static final String PLAIN_REVISED_FILE = "plain-revised.txt";
	
	private CommonDiff<String> commonDiff;
	private List<String> original;
	private List<String> revised;
	private Patch expectedPatch;

	protected void setUp() throws Exception {
		super.setUp();
		String originalFile = getFullFileName(PLAIN_ORIGINAL_FILE);
		String revisedlFile = getFullFileName(PLAIN_REVISED_FILE);
		commonDiff = new CommonDiff<String>();
		original = new StringReader(originalFile).readAsList();
		revised = new StringReader(revisedlFile).readAsList();
		expectedPatch = DiffUtils.diff(original, revised);
	}
	
	@Override
	protected void tearDown() throws Exception {
		commonDiff = null;
		commonDiff = new CommonDiff<String>();
		super.tearDown();
	}

	public void testDiffFromList() {
		commonDiff.diff(original, revised);
		assertEquals(expectedPatch.getDeltas(), commonDiff.getPatch().getDeltas());
	}

	public void testDiffFromReader() {
		commonDiff.diff(new StringReader(getFullFileName(PLAIN_ORIGINAL_FILE)), new StringReader(getFullFileName(PLAIN_REVISED_FILE)));
		assertEquals(expectedPatch.getDeltas(), commonDiff.getPatch().getDeltas());
	}

	public void testWrapResult() {
		HtmlDiffWrapper<String> wrapper = new HtmlDiffWrapper<String>();
		BaseWriter<?, String> writer = new StringWriterStdOut();
		commonDiff.wrapResult(writer, wrapper);
		commonDiff.diff(original, revised);
		commonDiff.wrapResult(writer, wrapper);
	}
	
	public String getFullFileName(String fileName) {
		return TestUtils.getFileFromResource(getClass(), fileName).getPath();
	}

}
