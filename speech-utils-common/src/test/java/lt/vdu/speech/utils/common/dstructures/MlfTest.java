package lt.vdu.speech.utils.common.dstructures;

import org.junit.Ignore;

import junit.framework.TestCase;

@Ignore
public class MlfTest extends TestCase {
	
	private Mlf mlfAEqual = new Mlf("20", "abab", "jjjj");
	private Mlf mlfBEqual = new Mlf("20", "abab", "jjjj");
	
	private Mlf mlfAInEqual = new Mlf("21", "bbbbbb", "kkkkk");
	private Mlf mlfBInEqual = new Mlf("12", "ccc", "l");

	public void testEqualsObject() {
		assertEquals(true, mlfAEqual.equals(mlfAEqual));
		assertEquals(true, mlfAEqual.equals(mlfBEqual));
		assertEquals(true, mlfBEqual.equals(mlfAEqual));
		
		assertEquals(false, mlfBInEqual.equals(mlfAInEqual));
		assertEquals(false, mlfAInEqual.equals(mlfBInEqual));
	}
	
	public void testHashCode() {
		assertTrue(mlfAEqual.equals(mlfBEqual) && (mlfAEqual.hashCode() == mlfBEqual.hashCode()));
		assertTrue(!mlfAInEqual.equals(mlfBEqual) && (mlfAInEqual.hashCode() != mlfBInEqual.hashCode()));
	}

}
