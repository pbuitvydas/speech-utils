package lt.vdu.speech.utils.common.config;

import java.net.URL;

import junit.framework.TestCase;

public class ConfigTest extends TestCase {
	
	private String fileName;
	private Config config;
	
	@Override
	protected void setUp() throws Exception {
		URL url = ClassLoader.getSystemResource("lt/vdu/speech/utils/common/config/settings.cfg");
		fileName = url.getFile();
		config = new Config(fileName);
		super.setUp();
	}

	public void testConfig() {

	}

	public void testGetStringValue() {
		String expected = config.getStringValue("STRING_VALUE");
		assertEquals(expected, "string value");
	}

	public void testGetValue() {
		Object expected = config.getStringValue("STRING_VALUE");
		assertEquals(expected, "string value");
	}

	public void testGetDoubleValue() {
		Double expected = config.getDoubleValue("DOUBLE_VALUE");
		assertEquals(expected, 12.5D);
	}

	public void testGetBooleanValue() {
		Boolean expected = config.getBooleanValue("BOOLEAN_TRUE_VAL");
		assertEquals(expected, Boolean.TRUE);
		expected = config.getBooleanValue("BOOLEAN _TRUE_VAL_LONG");
		assertEquals(expected, Boolean.TRUE);
		expected = config.getBooleanValue("BOOLEAN_FALSE_VAL");
		assertEquals(expected, Boolean.FALSE);
		expected = config.getBooleanValue("BOOLEAN _FALSE_VAL_LONG");
		assertEquals(expected, Boolean.FALSE);
	}

	public void testGetIntValue() {
		Integer expected = config.getIntValue("INT_VALUE");
		assertEquals(expected, new Integer(12));
	}

	public void testGetConfig() {

	}

}
