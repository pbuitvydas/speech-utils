package lt.vdu.speech.utils.common.diff;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import difflib.Chunk;
import difflib.Delta;
import difflib.Delta.TYPE;
import difflib.PatchFailedException;
import junit.framework.TestCase;
import lt.vdu.speech.utils.common.io.reader.StringReader;
import lt.vdu.speech.utils.common.utilities.TestUtils;

public class HtmlDiffWrapperTest extends TestCase {
	
	private HtmlDiffWrapper<String> wrapper;

	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		wrapper = new HtmlDiffWrapper<String>();
	}

	public void testWrapBegin() {
	}

	public void testWrap() {
		assertEquals("<tr></tr>", wrapper.wrap(null));
		Delta d = new Delta(null, null) {

			@Override
			public void applyTo(List<Object> arg0) throws PatchFailedException {
				// TODO Auto-generated method stub
				
			}

			@Override
			public TYPE getType() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void restore(List<Object> arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void verify(List<?> arg0) throws PatchFailedException {
				// TODO Auto-generated method stub
				
			}
			
		};
		assertEquals("<tr><td style=\"\"></td><td style=\"\">&nbsp;</td><td style=\"\"></td></tr>", wrapper.wrap(d));
	}

	public void testWrapEnd() {
	}

	public void testGetColor() {
		assertEquals("", wrapper.getColor(null));
	}

	public void testTdStringString() {
		assertEquals("<td style=\"\"></td>", wrapper.td(null, ""));
		assertEquals("<td style=\"\"></td>", wrapper.td("", null));
	}

	public void testGetRValue() {
			assertEquals("", wrapper.getRValue(null, null, null));
			//TODO more
		}
	
	public void testFileFormResource() {
		File fileCurrent = getFilefromResource("plain-original.txt");
		File fileExpeced = new File(ClassLoader.getSystemResource("lt/vdu/speech/utils/common/diff/plain-original.txt").getFile());
		assertEquals(fileExpeced.getPath(), fileCurrent.getPath());
	}
	
	public File getFilefromResource(String fileName) {
		return TestUtils.getFileFromResource(getClass(), fileName);
	}

}
