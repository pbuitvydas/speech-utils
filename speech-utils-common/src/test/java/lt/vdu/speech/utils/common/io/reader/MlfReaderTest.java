package lt.vdu.speech.utils.common.io.reader;

import java.io.File;
import java.util.List;

import org.junit.Ignore;

import junit.framework.TestCase;
import lt.vdu.speech.utils.common.dstructures.Mlf;
import lt.vdu.speech.utils.common.io.writer.BasicBufferedWriter;
import lt.vdu.speech.utils.common.utilities.TestUtils;

@Ignore
public class MlfReaderTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	public void testReadAsList() throws Exception {
		File f = TestUtils.getFileFromResource(getClass(), "standart.mlf");
		List<Mlf> list = new MlfReader(f.getAbsolutePath()).readAsList();
		BasicBufferedWriter<Mlf> writer = new BasicBufferedWriter<Mlf>(f.getAbsolutePath() + "2");
		writer.write(list);
		writer.close();
		
		f = TestUtils.getFileFromResource(getClass(), "malformed.mlf");
		list = new MlfReader(f.getAbsolutePath()).readAsList();
		writer = new BasicBufferedWriter<Mlf>(f.getAbsolutePath() + "2");
		writer.write(list);
		writer.close();
		
		f = TestUtils.getFileFromResource(getClass(), "withoutScore.mlf");
		list = new MlfReader(f.getAbsolutePath()).readAsList();
		writer = new BasicBufferedWriter<Mlf>(f.getAbsolutePath() + "2");
		writer.write(list);
		writer.close();
	}

}
