package lt.vdu.speech.utils.common.cli;

import junit.framework.TestCase;

public class CliUtilsTest extends TestCase {

	public void testGetExecutingResourceName() {
		String result = CliUtils.getExecutingResourceName(getClass());
		assertEquals(result, getClass().getSimpleName());
	}

}
