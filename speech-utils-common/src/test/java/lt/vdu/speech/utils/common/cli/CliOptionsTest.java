package lt.vdu.speech.utils.common.cli;

import java.io.ByteArrayOutputStream;

import org.apache.commons.cli.Options;

import junit.framework.TestCase;

public class CliOptionsTest extends TestCase {
	
	private CliOptions cliOptions;
	private Options options;
	private ByteArrayOutputStream byteArrayOutputStream;
	
	@Override
	protected void setUp() throws Exception {
		byteArrayOutputStream = new ByteArrayOutputStream();
		options = new Options();
		options.addOption("testOpt", false, "First option");
		options.addOption("testOpt2", true, "Second option with argument");
		cliOptions = new CliOptions(options, CliOptionsTest.class);
		super.setUp();
	}
	
	@Override
	protected void tearDown() throws Exception {
		byteArrayOutputStream.reset();
		super.tearDown();
	}

	public void testPrintUsage() {
		try {
			cliOptions.printUsage(getName(), options, byteArrayOutputStream);
			assertNotNull(byteArrayOutputStream.toString());
		} catch (Exception e) {
			fail("exception thrown at: " + getName() + "\n" + e.getMessage());
		}
	}

	public void testPrintHelpStd() {
		try {
			//cliOptions.printHelpStd();
		} catch (Exception e) {
			fail("exception thrown at: " + getName() + "\n" + e.getMessage());
		}
	
	}

	public void testPrintHelp() {
		try {
			cliOptions.printHelp(80, "", "", 2, 4, true, byteArrayOutputStream);
			assertNotNull(byteArrayOutputStream.toString());
		} catch (Exception e) {
			fail("exception thrown at: " + getName() + "\n" + e.getMessage());
		}
	}

}
