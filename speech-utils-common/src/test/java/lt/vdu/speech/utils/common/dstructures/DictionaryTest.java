package lt.vdu.speech.utils.common.dstructures;


import java.util.List;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DictionaryTest {
	
	private Dictionary dictionary;
	private DictEntry foo;
	private DictEntry bar;
	private DictEntry bar2;

	@Before
	public void setUp() throws Exception {
		dictionary = new Dictionary();
		foo = new DictEntry("foo", "f o o");
		bar = new DictEntry("bar", "b a r");
		bar2 = new DictEntry("bar", "b a r r");
		dictionary.put(foo);
		dictionary.put(bar);
		dictionary.put(bar2);
		dictionary.put(new DictEntry("a", "b"));
	}

	@Test
	public void testGetEntries()
	 throws Exception {
		List<DictEntry> entries = dictionary.getEntries(bar.getWord());
		assertEquals(2, entries.size());
		assertArrayEquals(entries.toArray(), new Object[] {bar, bar2});
		
	}

	@Test
	public void testPut()
	 throws Exception {
		dictionary.put(new DictEntry("a", "d"));
	}

	@Test
	public void testGetWords()
	 throws Exception {
		assertNotNull(dictionary.getWords());
	
	}

}
