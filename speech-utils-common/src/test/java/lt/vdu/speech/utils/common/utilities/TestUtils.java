package lt.vdu.speech.utils.common.utilities;

import java.io.File;
import java.net.URL;

public class TestUtils {
	
	public static File getFileFromResource(Class<?> clazz, String fileName) {
		String path = clazz.getPackage().getName();
		path = path.replace('.', '/');
		path += "/" + fileName;
		URL url = ClassLoader.getSystemResource(path);
		return new File(url.getFile());
	}

}
