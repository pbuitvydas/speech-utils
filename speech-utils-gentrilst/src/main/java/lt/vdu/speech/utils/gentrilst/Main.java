package lt.vdu.speech.utils.gentrilst;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lt.vdu.speech.utils.common.exception.SpeechUtilsException;
import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;

public class Main {
	
	private static final Logger log = LoggerFactory.getLogger(Main.class);

	public static void main(String[] args) throws SpeechUtilsException {
		if (args.length < 4 || args.length > 5) {
			printHelp();
			//test();
		} else {
			try {
				if (args.length == 5) {
					process(args[0], args[1], args[2], args[3], args[4]);
				} else {
					process(args[0], args[1], args[2], args[3], null);
				}
			} catch (SpeechUtilsException e) {
				ExceptionHandler.getInstance().handle(e);
			}
		}

	}
	
	public static void printHelp() {
		System.out.println("Generate list of unseen triphones for changed mlf file\n" 
				+ "usage: originalMlf revisedMlf dictionary triphonesList [hmmFile]\n" 
				+ "Note: triphonesList can be existing file in which generated new triphones will bee appended");
	}
	
	public static void test() throws SpeechUtilsException {
		process("D:/Speech/exp/ab_annotate/speech-synth/mlf/DEK_A - Copy.mlfw0"
				, "D:/Speech/exp/ab_annotate/speech-synth/mlf/DEK_A.mlfw0"
				, "D:/Speech/exp/ab_annotate/speech-synth/mono.dic"
				, "D:/Speech/exp/ab_annotate/speech-synth/synthlist"
				, null);
	}
	
	public static void process(String orgMlfF, String revMlfF, String dictionaryF, String outF, String hmmFile) throws SpeechUtilsException {
		TriListGenerator generator = new TriListGenerator();
		if (hmmFile != null) {
			generator.initDefinedPhonemes(hmmFile);
		}
		generator.process(orgMlfF, revMlfF, dictionaryF, outF);
	}

}
