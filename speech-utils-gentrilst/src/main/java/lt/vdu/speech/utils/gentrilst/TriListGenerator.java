package lt.vdu.speech.utils.gentrilst;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lt.vdu.speech.utils.common.diff.CommonDiff;
import lt.vdu.speech.utils.common.dstructures.DictEntry;
import lt.vdu.speech.utils.common.dstructures.Dictionary;
import lt.vdu.speech.utils.common.dstructures.Mlf;
import lt.vdu.speech.utils.common.exception.SpeechUtilsException;
import lt.vdu.speech.utils.common.exception.WordNotFoundException;
import lt.vdu.speech.utils.common.exception.handler.ExceptionHandler;
import lt.vdu.speech.utils.common.io.reader.BasicBufferedReader;
import lt.vdu.speech.utils.common.io.reader.DictionaryReader;
import lt.vdu.speech.utils.common.io.reader.MlfReader;
import lt.vdu.speech.utils.common.io.reader.StringReader;
import lt.vdu.speech.utils.common.io.writer.BasicBufferedWriter;
import lt.vdu.speech.utils.common.io.writer.DictionaryWriter;
import lt.vdu.speech.utils.common.utilities.ClassUtils;
import difflib.Delta;

public class TriListGenerator {
	
	private static final Logger log = LoggerFactory.getLogger(TriListGenerator.class);
	
	private Set<String> triphonesSet = new HashSet<String>();
	private CommonDiff<Mlf> differ;
	private Dictionary dictionary;
	private List<String> definedPhonemesList;
	private boolean dictionaryChanged;
	
	public void initDefinedPhonemes(String hmmFile) {
		
		log.info("Reading defined phonemes from {}", hmmFile);
		StringReader sr = new StringReader(hmmFile) {

			@Override
			public List<String> readAsList() {
				List<String> retList = new ArrayList<String>();
				String line;
				try {
					while ((line = readItem()) != null) {
						if (line.contains("~t")) {
							line = line.replace("~t \"T_", "");
							line = line.substring(0, line.length() - 1);
							retList.add(normaliseString(line));
						}
					}
				} catch (Exception e) {
					ExceptionHandler.getInstance().handle(e);
				}
				return retList;
			}

		};
		definedPhonemesList = sr.readAsList();
		log.info("Done reading {} defined phonemes", definedPhonemesList.size());
		sr.close();
 		
	}
	
	public void process(String orgMlfF, String revMlfF, String dictionaryF, String outF) throws SpeechUtilsException {
		initTriphionesSet(outF);
		log.info("Reading dictionary from '{}'", dictionaryF);
		DictionaryReader dReader = new DictionaryReader(dictionaryF);
		dictionary = dReader.readDictionary();
		dReader.close();
		log.info("Done reading {} dictionary entries", dictionary.getEntryCount(),  dictionaryF);

		log.info("Searching for diferences betwen '{}' '{}'", orgMlfF, revMlfF);
		differ = new CommonDiff<Mlf>();
		MlfReader orgReader = new MlfReader(orgMlfF);
		MlfReader revReader = new MlfReader(revMlfF);
		differ.diff(orgReader, revReader);
		orgReader.close();
		revReader.close();
		
		log.info("Generating unseen triphpnes...");
		for (Delta d : differ.getPatch().getDeltas()) {
			int chgIdx = d.getRevised().getPosition();
			for (Mlf mlf : getMlfs(d.getRevised().getLines())) {
				log.debug("Processing {} {}", mlf.getLabName(), mlf);
				generateTriphones(chgIdx++);
			}
		}
		
		log.info("Writing resulting {} triphones to '{}'", triphonesSet.size(), outF);
		BasicBufferedWriter<String> writer = new BasicBufferedWriter<String>(outF);
		for (String str : triphonesSet) {
			writer.write(str);
		}
		writer.close();
		if (dictionaryChanged) {
			log.info("Dictionary has changed, writing updated dictionary to {}", dictionaryF);
			DictionaryWriter dw = new DictionaryWriter(dictionaryF);
			dw.writeDicitonary(dictionary, true);
			dw.close();
		}
		log.info("Done.");
	}
	
	public void initTriphionesSet(String triFile) {
		if (new File(triFile).exists()) {
			try {
				log.info("Reading contents of triphonesList {}", triFile);
				BasicBufferedReader<String> reader = new StringReader(triFile);
				List<String> list = reader.readAsList();
				log.info("Done reading {} triphones", list.size());
				for (String str : list) {
					triphonesSet.add(str);
				}
				reader.close();
			} catch (Exception e) {
				ExceptionHandler.getInstance().handle(e);
			}
		}
	}
	
	public void generateTriphones(int chnageIndex) {
			List<String> words = new ArrayList<String>();
			addFront(words, chnageIndex);
			checkForUndefinedPhonemes(getMlfStringAtIndex(chnageIndex));
			words.add(getMlfStringAtIndex(chnageIndex));
			addBack(words, chnageIndex);
			combinateWords(words);
	}
	
	public void addFront(List<String> list, int idx) {
		String mlf = getMlfStringAtIndex(idx - 1);
		list.add(0, mlf);
		if (!mlf.equals("sill")) {
			mlf = getMlfStringAtIndex(idx - 2);
			list.add(0, mlf);
		}
	}
	
	public void addBack(List<String> list, int idx) {
		String mlf = getMlfStringAtIndex(idx + 1);
		list.add(mlf);
		if (!mlf.equals("sill")) {
			mlf = getMlfStringAtIndex(idx + 2);
			list.add(mlf);
		}
	}
	
	public void combinateWords(List<String> words) {
		log.debug("Generating triphones from {}", words);
		for (int i = 2; i < words.size(); i++) {
			populateContextFromWords(words.get(i - 2), words.get(i - 1), words.get(i));
		}
	}
	
	protected void checkForUndefinedPhonemes(String word) {
		if (definedPhonemesList == null) {
			return;
		}
		try {
			List<DictEntry> prons = dictionary.getEntries(word);
			for (int i = 0; i < prons.size(); i++) {
				DictEntry entry = prons.get(i); 
				for (String phoneme : entry.getTranscriptArray()) {
					if (!definedPhonemesList.contains(phoneme)) {
						log.warn("Tried to generate triphone with undefined phonemes, phoneme {} not found in hmm", phoneme);
						log.warn("To avoid possible erors entry {} wil be removed from dictionary", entry);
						if (dictionary.getEntries(word).size() == 1) {
							ExceptionHandler.getInstance().handle(new SpeechUtilsException("Tried to remove last entry" + entry + "from dictionary"
									+ "\nThis means that this particular entry should be removed from mlf file"));
						} else {
							dictionary.remove(entry);
							dictionaryChanged = true;
						}
					}
				}
			}
		} catch (WordNotFoundException e) {
			ExceptionHandler.getInstance().handle(e);
		}
	}
	
	protected void checkForUndefinedPhonemes(List<String> words) {
		if (definedPhonemesList == null) {
			return;
		}
		try {
			for (String word : words) {
				List<DictEntry> prons = dictionary.getEntries(word);
				for (DictEntry entry : prons) {
					for (String phoneme : entry.getTranscriptArray()) {
						if (!definedPhonemesList.contains(phoneme)) {
							log.warn("Tried to generate triphone with undefined phonemes, phoneme {} not found in hmm", phoneme);
							log.warn("To avoid possible erors entry {} wil be removed from dictionary", entry);
							if (dictionary.getEntries(word).size() == 1) {
								ExceptionHandler.getInstance().handle(new SpeechUtilsException("Tried to remove last entry" + entry + "from dictionary"
										+ "\nThis means that this particular entry should be removed from mlf file"));
							} else {
								dictionary.remove(entry);
								dictionaryChanged = true;
							}
						}
					}
				}
			}
		} catch (WordNotFoundException e) {
			ExceptionHandler.getInstance().handle(e);
		}
	}
	
	public void populateContextFromWords(String leftCtx, String middleCtx, String rightCtx) {
		try {
			leftCtx = normaliseString(leftCtx);
			middleCtx = normaliseString(middleCtx);
			rightCtx = normaliseString(rightCtx);
			for (DictEntry left : dictionary.getEntries(leftCtx)) {
				for (DictEntry middle : dictionary.getEntries(middleCtx)) {
					for (DictEntry right : dictionary.getEntries(rightCtx)) {
						List<String> list = new ArrayList<String>(left.getTranscriptList());
						list.addAll(middle.getTranscriptList());
						list.addAll(right.getTranscriptList());
						genTriphones(list);
					}
				}
			}
		} catch (Exception e) {
			ExceptionHandler.getInstance().handle(e);
		}

	}
	
	protected void genTriphones(List<String> phonemes) {
		for (int i = 2; i < phonemes.size(); i++) {
			String triphone = String.format("%s-%s+%s", phonemes.get(i - 2), phonemes.get(i - 1), phonemes.get(i));
			if (triphone.contains("-sil+")) {
				log.debug("Generated context dependent sil: {} skipping", triphone);
				continue;
			}
			log.trace("triphone: {}", triphone);
			triphonesSet.add(triphone);
		}
	}
	
	public String getMlfStringAtIndex(int index) {
		String str = differ.getRevised().get(index).getItem();
		str = normaliseString(str);
		return str;
	}
	
	public String normaliseString(String str) {
		if (str.startsWith("'") && str.endsWith("'")) {
			str = str.substring(1, str.length() -1);
		}
		while (str.contains("\"") && !str.contains("\\")) {
			str = str.replace("\"", "\\\"");
		}
		return str;
	}
	
	public List<Mlf> getMlfs(List<?> lst) throws SpeechUtilsException {
		log.trace("Extracting mlf from changset: {}", lst);

		if (lst == null) {
			throw new SpeechUtilsException("changed mlf file possibly has deletions");
		}
		if (lst.size() == 0) {
			throw new SpeechUtilsException("changed mlf file possibly has deletions");
		}
		List<Mlf> mlfList = ClassUtils.unwild(lst);
		return mlfList;
	}

}
